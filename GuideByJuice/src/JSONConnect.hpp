/* Copyright (c) 2012 Research In Motion Limited.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef JSONConnect_HPP
#define JSONConnect_HPP

#include <bb/cascades/GroupDataModel>
#include <bb/pim/contacts/ContactService>

#include <QtCore/QObject>

/**
 * @short The controller class that makes access to contacts available to the UI.
 */
//! [0]
class JSONConnect : public QObject
{
    Q_OBJECT

public:
    JSONConnect(QObject *parent = 0);
    bool RequestAreaData();      //Areas function
    bool RequestPromosData();      //Promos function
    bool RequestContestsData();      //Contests function
    bool RequestAreaIdData(QString area_id);      //AreaId function
    bool RequestBarMonthData();      //Bar Month function
    bool RequestClubMonthData();      //Club Month function
    bool RequestSearchData(QString keyword);      //Search function
    bool RequestNearByData(QString lat, QString logu, QString radius);      //Nearby function
    QObject* m_pParent;

public Q_SLOTS:
    /**
     * Marks the contact with the given @p indexPath as current.
     */

Q_SIGNALS:
    // The change notification signal for the property

private Q_SLOTS:
    // Filters the contacts in the model according to the filter property
    //void requestFinished(QNetworkReply *reply);

private:
    // The accessor methods of the properties
    bool NetworkCheck();		//network check
    QVariantList qlist;
    QString AREA_URL;
    QString PROMOS_URL;
    QString CONTESTS_URL;
    QString AREAID_URL;
    QString BARMONTH_URL;
    QString CLUBMONTH_URL;
    QString SEARCH_URL;
    QString NEARBY_URL;
};
//! [0]

#endif
