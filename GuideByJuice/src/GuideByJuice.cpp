// Tabbed pane project template
#include "GuideByJuice.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/Tab>
#include <QList.h>
#include <bb/system/phone/Phone>
#include <bb/data/DataSource>
#include <QUrl>
#include "WebImageView.h"
#include "QmlEventHandler.h"
#include <bb/platform/RouteMapInvoker>
#include <qt4/QtCore/QTimer.h>
//#include "InvokeCoreApps.h"

using namespace bb::cascades;

GuideByJuice::GuideByJuice(bb::cascades::Application *app)
: QObject(app)
{
    // simon - reg WebImageView
    qmlRegisterType<bb::platform::RouteMapInvoker>("bb.platform", 1, 0, "RouteMapInvoker");
	qmlRegisterType<WebImageView>("org.labsquare", 1, 0, "WebImageView");
	qmlRegisterType<QmlEventHandler>("my.lib", 1, 0, "QmlEventHandler");
	qmlRegisterType<QmlEventHandler>("my.QVariantMap", 1, 0, "QVariantMap");
	//qmlRegisterType<QTimer>("bb.cascades", 1, 0, "QTimer");
	//qmlRegisterType<QTimer>("my.library", 1, 0, "QTimer");
	//	qmlRegisterType<QUrl>("org.labsquare", 1, 0, "QUrl");
//	qmlRegisterType<InvokeCoreApps>("my.lib", 1, 0, "InvokeCoreApps");


	qmlRegisterType<bb::system::phone::Phone>("bb.system.phone", 1, 0, "Phone");
	bb::data::DataSource::registerQmlTypes();

    // create scene document from main.qml asset
    // set parent to created document to ensure it exists for the whole application lifetime
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    QmlEventHandler * pHandler = new QmlEventHandler(this);
    qml->setContextProperty("eventHandler", pHandler);

    pHandler->onInitial(0);

    // create root object for the UI
    root = qml->createRootObject<AbstractPane>();

    // set created root object as a scene
    m_pApp = app;

    //Create an instance of our PushManager class. This class will handle all push interactions.
    m_pushManager = new PushManager(app);
    //Whenever the push data is changed (new push received, data store cleared) we need to update the UI
    // this connection allows us to be notified so we can do just that.
    //connect(m_pushManager, SIGNAL(pushListChanged(const QVariantList &)), this, SLOT(pushListUpdated(const QVariantList &)));

    //This call triggers a refresh of the push messages pulled from QSettings
    m_pushManager->updatePushList();
}

NavigationPane* GuideByJuice::getHomeNavPane(){
	return root->findChild<NavigationPane*>("homeNav");
}

NavigationPane* GuideByJuice::getVenuesNavPane(){
	return root->findChild<NavigationPane*>("venuesNav");
}

NavigationPane* GuideByJuice::getPromoNavPane(){
	return root->findChild<NavigationPane*>("promoNav");
}

NavigationPane* GuideByJuice::getSearchNavPane(){
	return root->findChild<NavigationPane*>("searchNav");
}

//by KJB
NavigationPane* GuideByJuice::getAroundmeNavPane(){
	return root->findChild<NavigationPane*>("aroundmeNav");
}

AbstractPane* GuideByJuice::getAbstractPane() {
	return root;
}

void GuideByJuice::ShowHome() {
	m_pApp->setScene(root);
}

void GuideByJuice::ShowBootup() {
    // Show the bootup_2
	QmlDocument *qml = QmlDocument::create("asset:///bootup_2.qml").parent(this);

	QmlEventHandler * pHandler = new QmlEventHandler(this);
	qml->setContextProperty("eventHandler", pHandler);

	m_pApp->setScene(qml->createRootObject<AbstractPane>());
}
