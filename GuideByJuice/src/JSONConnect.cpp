/* Copyright (c) 2012 Research In Motion Limited.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "JSONConnect.hpp"

#include <bb/data/JsonDataAccess>
using namespace bb::cascades;
using namespace bb::data;
using namespace bb::pim::contacts;

//! [0]
JSONConnect::JSONConnect(QObject *parent)
	: QObject(parent)
{
    //if(NetworkCheck()){
    	//RequestServer(QString("http://223.25.247.242/api/1/venues/areas/"));
    //}
	AREA_URL = "http://223.25.247.242/api/1/venues/areas/";
    PROMOS_URL = "http://223.25.247.242/api/1/carlsberg/promos/";
    CONTESTS_URL = "http://223.25.247.242/api/1/carlsberg/contests/";
    AREAID_URL = "http://223.25.247.242/api/1/venues/area/";
    BARMONTH_URL = "http://223.25.247.242/api/1/venues/botm/";
    CLUBMONTH_URL = "http://223.25.247.242/api/1/venues/cotm/";
    SEARCH_URL = "http://223.25.247.242/api/1/venues/search/?key=";
    NEARBY_URL = "http://223.25.247.242/api/1/venues/aroundme/?";
    m_pParent = parent;
}
//! [0]

//! [1]
//network connect check function
bool JSONConnect::NetworkCheck(){
	QNetworkConfigurationManager m;
/*	if(m.isOnline()){
		return true;
	}else{
		bool available = false;
		foreach(QNetworkInterface
		interface,QNetworkInterface::allInterfaces())
		{
			if(interface.flags().testFlag(QNetworkInterface::IsUp) &&
			interface.name().compare(QString("ti0")) == 0)
			{
				available = true;
			}
		}
		return available;
	}
*/	return true;
}
//Retrieve list of all areas
bool JSONConnect::RequestAreaData(){
	//network connect check
	if(NetworkCheck()){
		QNetworkRequest request = QNetworkRequest();
		QUrl url = QUrl(AREA_URL);
		request.setUrl(url);
		QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
		bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
		               m_pParent, SLOT(requestFinished(QNetworkReply*)));

		// Sends the HTTP request.
		networkAccessManager->get(request);

		return true;
	}else{
		return false;
	}
}

//Retrieve Carlsberg Promos
bool JSONConnect::RequestPromosData(){
	//network connect check
	if(NetworkCheck()){
		QNetworkRequest request = QNetworkRequest();
		request.setUrl(QUrl(PROMOS_URL));
		QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
		bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
		               m_pParent, SLOT(requestFinished(QNetworkReply*)));

		// Sends the HTTP request.
		networkAccessManager->get(request);

		return true;
	}else{
		return false;
	}
}

//Retrieve Carlsberg Contests
bool JSONConnect::RequestContestsData(){
	//network connect check
	if(NetworkCheck()){
		QNetworkRequest request = QNetworkRequest();
		request.setUrl(QUrl(CONTESTS_URL));
		QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
		bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
		               m_pParent, SLOT(requestFinished(QNetworkReply*)));

		// Sends the HTTP request.
		networkAccessManager->get(request);

		return true;
	}else{
		return false;
	}
}

//Retrieve list of all venues in an area
bool JSONConnect::RequestAreaIdData(QString area_id){
	//network connect check
	if(NetworkCheck()){
		QString url = AREAID_URL + area_id + QString("/");
		QNetworkRequest request = QNetworkRequest();
		request.setUrl(QUrl(url));
		QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
		bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
		               m_pParent, SLOT(requestFinished(QNetworkReply*)));

		// Sends the HTTP request.
		networkAccessManager->get(request);

		return true;
	}else{
		return false;
	}
}

//Retrieve Bar of the Month
bool JSONConnect::RequestBarMonthData(){
	//network connect check
	if(NetworkCheck()){
		QNetworkRequest request = QNetworkRequest();
		request.setUrl(QUrl(BARMONTH_URL));
		QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
		bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
		               m_pParent, SLOT(requestFinished(QNetworkReply*)));

		// Sends the HTTP request.
		networkAccessManager->get(request);

		return true;
	}else{
		return false;
	}
}

//Retrieve Club of the Month
bool JSONConnect::RequestClubMonthData(){
	//network connect check
	if(NetworkCheck()){
		QNetworkRequest request = QNetworkRequest();
		request.setUrl(QUrl(CLUBMONTH_URL));
		QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
		bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
		               m_pParent, SLOT(requestFinished(QNetworkReply*)));

		// Sends the HTTP request.
		networkAccessManager->get(request);

		return true;
	}else{
		return false;
	}
}

//Search
bool JSONConnect::RequestSearchData(QString keyword){
	//network connect check
	if(NetworkCheck()){
		QString url = SEARCH_URL + keyword;
		QNetworkRequest request = QNetworkRequest();
		request.setUrl(QUrl(url));
		QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
		bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
		               m_pParent, SLOT(requestFinished(QNetworkReply*)));

		// Sends the HTTP request.
		networkAccessManager->get(request);

		return true;
	}else{
		return false;
	}
}

//Search for venues nearby
bool JSONConnect::RequestNearByData(QString lat, QString logu, QString radius){
	//network connect check
	if(NetworkCheck()){
		QString url = NEARBY_URL + QString("lat=") + lat + QString("&lng=");
		url = url + logu;
		url = url + QString("&r=");
		url = url + radius;
		qDebug()<<"===="<<url<<"====";
		QNetworkRequest request = QNetworkRequest();
		request.setUrl(QUrl(url));
		QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
		bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
		               m_pParent, SLOT(requestFinished(QNetworkReply*)));

		// Sends the HTTP request.
		networkAccessManager->get(request);

		return true;
	}else{
		return false;
	}
}

/*
//Finish SIGNAL Function
void JSONConnect::requestFinished(QNetworkReply *reply)
{

	if (!reply->error()) {
		qDebug() << "Got network data";
		// Let's get ALL the data
		const QByteArray response(reply->readAll());

		JsonDataAccess jsondata;
		//QVariantMap results = jsondata.loadFromBuffer(response).toMap();
		QVariant results = jsondata.loadFromBuffer(response);
		QVariantList qvl = results.value<QVariantList>();
		//qlist.append(qvl);
		for(int i = 0; i < qvl.length(); i++){
			QVariantMap map = qvl.at(i).toMap();
			QString s = map.value("name").toString();
			QString s1 = map.value("id").toString();
			//qDebug() << s;
		}
		//emit networkReply(results);

	} else {
		//qDebug() << "Got network error";
		//qlist.clear();
		//emit networkError();
	}

	// Cleanup
	reply->deleteLater();
}
*/
//! [7]
