// Tabbed pane project template
#ifndef GuideByJuice_HPP_
#define GuideByJuice_HPP_

#include <QObject>
#include <bb/cascades/NavigationPane>
#include "PushManager.hpp"

using namespace bb::cascades;

namespace bb { namespace cascades { class Application; }}

/*!
 * @brief Application pane object
 *
 *Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class GuideByJuice : public QObject
{
    Q_OBJECT
public:
    GuideByJuice(bb::cascades::Application *app);
    virtual ~GuideByJuice() {}

    NavigationPane* getHomeNavPane();
    NavigationPane* getVenuesNavPane();
    NavigationPane* getSearchNavPane();
    NavigationPane* getPromoNavPane();
    NavigationPane* getAroundmeNavPane();//by kjb
    AbstractPane* getAbstractPane();
    void ShowHome();
    void ShowBootup();
private:
    AbstractPane *root;
    Application *m_pApp;

private:
	PushManager *m_pushManager; //Handle all push interactions

};

#endif /* GuideByJuice_HPP_ */
