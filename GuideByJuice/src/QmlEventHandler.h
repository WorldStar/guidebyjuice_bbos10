/*
 * QmlEventHandler.h
 *
 *  Created on: Apr 19, 2013
 *      Author: Simon
 */

#ifndef QMLEVENTHANDLER_H_
#define QMLEVENTHANDLER_H_

#include <QObject>
#include "GuideByJuice.hpp"
#include "JSONConnect.hpp"
#include "WebImageView.h"
#include <bb/cascades/ListView>
#include <bb/cascades/ActivityIndicator>

#define REQUEST_BAR_MONTH    0
#define REQUEST_CLUB_MONTH   1
#define REQUEST_NEARBY       2
#define REQUEST_AREAS        3
#define REQUEST_AREA_ID      4
#define REQUEST_SEARCH       5
#define REQUEST_PROMOS       6
#define REQUEST_CONTESTS     7
#define REQUEST_SEARCH_LOCATION     8
#define REQUEST_SELECT_LOCATION     9
#define REQUEST_INITIAL_BAR     10
#define REQUEST_INITIAL_CLUB     11

#define REQUEST_AROUNDME     12//by KJB
#define REQUEST_AROUNDMEITEM 13//by KJB
#define REQUEST_AREAS_FOR_LISTCACHING     14

class QmlEventHandler : public QObject {
	Q_OBJECT
public:
	QmlEventHandler( );
	QmlEventHandler( QObject * );
	virtual ~QmlEventHandler();

	Q_INVOKABLE void onTouchVenuesTab(QObject* );
	Q_INVOKABLE void onTouchPromoTab(QObject* );
	Q_INVOKABLE void onTouchSearchTab();

	void onInitial(int stage);
	Q_INVOKABLE void ShowHome();
	Q_SLOT void ShowHomeAuto();
	Q_INVOKABLE void startCountDown();
	Q_INVOKABLE void onTouchBarOfMonth();
	Q_INVOKABLE void onTouchClubOfMonth();
	Q_INVOKABLE void onTouchAreaItem(QObject*, QObject*, QObject*);
	Q_INVOKABLE void onTouchMerchantItem(QObject*);
	Q_INVOKABLE void onSearch(QObject*,QObject*);
	Q_INVOKABLE void onSelectArea(QObject*,QObject*);
	Q_INVOKABLE void onTouchPromo(QObject*);

	Q_INVOKABLE void OnTouchImageSlider(WebImageView*);
	Q_INVOKABLE void onTouchReview();

	/////////////////////////////////////////
	/////////AroundMe by KJB/////////////////
	Q_INVOKABLE void onTouchAroundmeTab(QObject* indicator);
    Q_INVOKABLE QVariantList worldToPixelInvokable(QObject* mapObject, double latitude, double longitude) const;
    Q_INVOKABLE void updateMarkers(QObject* mapObject, QObject* containerObject) const;
    Q_INVOKABLE void onChangeAroundMe(QObject* indicator, double lat, double logu, double radius);
    Q_INVOKABLE void onTouchAroundMeItem(QObject* indicator, QString venueName);
    Q_INVOKABLE void Sleep(int i, int sender);
    Q_SIGNALS:
        void UpdateAroundme(QObject* dataGroup);
    Q_SIGNALS:
        void InitializeAroundMe();
        void ShowenMapView();
    Q_SIGNALS:
        void addPintoContainer(int i, int idnum, QString name, double lat, double lon);
        void removePins();
        void showErrorLabel();
        /////////////////////////////////////////

public Q_SLOTS:
    // Filters the contacts in the model according to the filter property
    void requestFinished(QNetworkReply *reply); // insert this

public:
    void ShowMerchantDetails(NavigationPane * pane, bool hasSlider);
    void UpdateHomeList();
    void UpdateVenuesList();
    void UpdatePromoList();
    void ShowMerchantList(NavigationPane * pane, QString);
    void UpdateLocationList(NavigationPane * pane);

    //////////////////////////////////////////
    //////////////AroundMe By KJB/////////////
    void UpdateAroundMeList();
    Q_SLOT void ShowMapView();
    //////////////////////////////////////////

    void lock();
    void lock(QObject*);
    Q_INVOKABLE void unlock();
private:
    int m_RequestType;
	GuideByJuice* m_pMainInstance;
	JSONConnect *m_pJSONConnector;
    QVariantList m_QVL;  //insert this
    bool m_lock;

    QVariantMap *m_pCachedContents;
    ActivityIndicator* m_pIndicator;
    /////////////////////////////////////////
    ///////////AroundMe by KJB///////////////
private:
    QPoint worldToPixel(QObject* mapObject, double latitude, double longitude) const;
    /////////////////////////////////////////

};

#endif /* QMLEVENTHANDLER_H_ */
