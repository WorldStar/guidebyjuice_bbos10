/*
 * WebImageView.h
 *
 *  Created on: 4 oct. 2012
 *      Author: schutz
 */

#ifndef WEBIMAGEVIEW_H_
#define WEBIMAGEVIEW_H_

#include <bb/cascades/ImageView>
#include <QNetworkAccessManager>
#include <QUrl>
#include <bb/cascades/Container>

using namespace bb::cascades;

class QmlEventHandler;

class WebImageView: public bb::cascades::ImageView {
	Q_OBJECT
	Q_PROPERTY (QUrl url READ url WRITE setUrl NOTIFY urlChanged)

public:
	WebImageView();
	const QUrl& url() const;
//	const QString clickable() const;

	public Q_SLOTS:
	void setUrl(const QUrl& url);

	public Q_SLOTS:

//	public Q_SLOTS:
	void setClickable(const bool clickable);

	private Q_SLOTS:
	void imageLoaded();

	Q_INVOKABLE void onTouch(bb::cascades::TouchEvent*);

	signals:
	void urlChanged();

	Q_SIGNALS:
	void imageDownloaded();

public:
	void setEventHandler(QmlEventHandler*);
	void setLargeImageUrl(const QUrl& url);
	const QUrl& largeImageUrl();

private:
	static QNetworkAccessManager * mNetManager;
	QUrl mUrl, m_LargeUrl;
	bool m_Clickable;
	QmlEventHandler* m_EventHandler;
};

#endif /* WEBIMAGEVIEW_H_ */
