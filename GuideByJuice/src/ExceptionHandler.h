/*
 * ExceptionHandler.h
 *
 *  Created on: Apr 20, 2013
 *      Author: Simon
 */

#ifndef EXCEPTIONHANDLER_H_
#define EXCEPTIONHANDLER_H_

#include <qobject.h>

#define ERROR_RECEIVEDATA_EMPTY	10001
#define ERROR_VENUE_INVALID		10002
#define ERROR_PHOTOLIST_INVALID	10003
#define ERROR_MERCHANT_INVALID	10004
#define ERROR_SEARCHRESULT_EMPTY	10005
#define ERROR_SEARCHKEYWORD_EMPTY	10006
#define ERROR_NETWORK_INVALID	10007
#define ERROR_INVALID_RESPONSE	10008

class ExceptionHandler: public QObject {
public:
	ExceptionHandler();
	virtual ~ExceptionHandler();

	static void ShowError(int err_type);
};

#endif /* EXCEPTIONHANDLER_H_ */
