/*
 * QmlEventHandler.cpp
 *
 *  Created on: Apr 19, 2013
 *      Author: Simon
 */

#include "common.h"
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/InvokeQuery>
#include <bb/cascades/Invocation>
#include <bb/cascades/ScrollView>
#include <bb/cascades/DropDown>
#include <bb/cascades/Option>
#include <bb/cascades/Label>
#include <bb/cascades/TextField>
#include <bb/data/JsonDataAccess>
#include <bb/platform/geo/Point.hpp>//by KJB
#include <bb/cascades/maps/MapView>//by KJB
#include "ExceptionHandler.h"
#include "WebImageView.h"
#include <bb/system/SystemDialog>
#include "QmlEventHandler.h"

using namespace bb::data;
using namespace bb::system;
using namespace bb::cascades;//by KJB
using namespace bb::cascades::maps;//by KJB
using namespace bb::platform::geo;//by KJB
using namespace bb::cascades;//by KJB
using namespace bb::cascades::maps;//by KJB
using namespace bb::platform::geo;//by KJB

QmlEventHandler::QmlEventHandler() {
	m_lock = false;
	m_pCachedContents = new QVariantMap();

	m_pJSONConnector = new JSONConnect(this);

	m_pIndicator = 0x0;
}

QmlEventHandler::QmlEventHandler(QObject * parent = 0) {
	m_pMainInstance = (GuideByJuice*) parent;
	m_lock = false;
	m_pCachedContents = new QVariantMap();

	m_pJSONConnector = new JSONConnect(this);

	m_pIndicator = 0x0;
}

QmlEventHandler::~QmlEventHandler() {
}

void QmlEventHandler::lock() {
	m_lock = true;
}

void QmlEventHandler::lock(QObject* pIndicatorParent) {
	m_lock = true;

//	m_pIndicator = new ActivityIndicator((Container*)pIndicatorParent);
	m_pIndicator = (ActivityIndicator*)pIndicatorParent;

	m_pIndicator->start();
}

void QmlEventHandler::unlock() {
	m_lock = false;

	if(m_pIndicator != 0x0)
		m_pIndicator->stop();

}

void QmlEventHandler::onInitial(int stage = 0) {
	switch(stage) {
	case 0:
		if(m_pJSONConnector->RequestBarMonthData())
			m_RequestType = REQUEST_INITIAL_BAR;
		else {
			ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
			unlock();
		}
		break;
	case 1:
		if(m_pJSONConnector->RequestClubMonthData())
			m_RequestType = REQUEST_INITIAL_CLUB;
		else {
			ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
			unlock();
		}
		break;
	}
}
void QmlEventHandler::onTouchBarOfMonth() {
	if (m_lock)
		return;
	lock();

	// load data

	if(m_pJSONConnector->RequestBarMonthData())
		m_RequestType = REQUEST_BAR_MONTH;
	else {
		ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
		unlock();
	}
}

void QmlEventHandler::onTouchClubOfMonth() {
	if (m_lock)
		return;
	lock();

	if(m_pJSONConnector->RequestClubMonthData())
		m_RequestType = REQUEST_CLUB_MONTH;
	else {
		ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
		unlock();
	}
}

// receive JSON Data.
void QmlEventHandler::requestFinished(QNetworkReply *reply) {

	QDateTime dt;
	NavigationPane * pane;
	QString title;

	if (!reply->error()) {
//		qDebug() << "Create date";
		// Let's get ALL the data
		const QByteArray response(reply->readAll());
		JsonDataAccess jsondata;

		QVariant results = jsondata.loadFromBuffer(response), item;

		m_QVL = results.value<QVariantList>();

		if (m_QVL.isEmpty() && m_RequestType != REQUEST_AROUNDME) {//by KJB
			if (m_RequestType == REQUEST_SEARCH)
				ExceptionHandler::ShowError(ERROR_SEARCHRESULT_EMPTY);
			else
				ExceptionHandler::ShowError(ERROR_RECEIVEDATA_EMPTY);

			// Cleanup
			reply->deleteLater();

			unlock();

			return;
		}

		if (m_pCachedContents->contains("curNavPane"))
			m_pCachedContents->remove("curNavPane");

		switch (m_RequestType) {
		case REQUEST_INITIAL_BAR:
			if (m_pCachedContents->contains("BarOfMonth"))
				m_pCachedContents->remove("BarOfMonth");
			m_pCachedContents->insert("BarOfMonth", m_QVL);

			onInitial(1);

			break;
		case REQUEST_INITIAL_CLUB:
			if (m_pCachedContents->contains("ClubOfMonth"))
				m_pCachedContents->remove("ClubOfMonth");
			m_pCachedContents->insert("ClubOfMonth", m_QVL);

			m_pCachedContents->insert("curNavPane", "home");

			UpdateHomeList();
			break;
		case REQUEST_BAR_MONTH:
		case REQUEST_CLUB_MONTH:
			pane = m_pMainInstance->getHomeNavPane();
			ShowMerchantDetails(pane, true);
			break;
		case REQUEST_AREAS:
			UpdateVenuesList();
			break;
		case REQUEST_PROMOS:
			m_pCachedContents->insert("curNavPane", "promo");
			UpdatePromoList();
			break;
		case REQUEST_AREA_ID:
			pane = m_pMainInstance->getVenuesNavPane();
			m_pCachedContents->insert("curNavPane", "venues");

			title = m_pCachedContents->value("seledKeyword").toString();

			if(title.length() > 16)
				title = title.left(15) + "...";

			ShowMerchantList(pane, title);
			break;
		case REQUEST_SELECT_LOCATION:
			title = m_pCachedContents->value("seledKeyword").toString();
		case REQUEST_SEARCH:
			pane = m_pMainInstance->getSearchNavPane();
			m_pCachedContents->insert("curNavPane", "search");

			title = "Search";
			ShowMerchantList(pane, title);
			break;
		case REQUEST_AREAS_FOR_LISTCACHING:
			if (m_pCachedContents->contains("areaList"))
				m_pCachedContents->remove("areaList");
			m_pCachedContents->insert("areaList", m_QVL);

			break;
		case REQUEST_SEARCH_LOCATION:
			pane = m_pMainInstance->getSearchNavPane();
			m_pCachedContents->insert("curNavPane", "search");

			UpdateLocationList(pane);

			break;
		/////////////////////////////////////////////////////////////
		//////////AroundMe by KJB////////////////////////////////////
		case REQUEST_AROUNDME:
			qDebug()<<"====Selected AroundMe====";
			m_pCachedContents->insert("curNavPane", "aroundme");
//			pane = m_pMainInstance->getSearchNavPane();
//			m_pCachedContents->insert("curNavPane", "search");
			UpdateAroundMeList();

			// call area list
			if(m_pJSONConnector->RequestAreaData()) {
				m_RequestType = REQUEST_AREAS_FOR_LISTCACHING;
			}
			else {
				ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
				unlock();
			}

			break;
		case REQUEST_AROUNDMEITEM:
			pane = m_pMainInstance->getAroundmeNavPane();
			m_pCachedContents->insert("curNavPane", "aroundme");

			title = QString("Keyword \"")
					+ m_pCachedContents->value("seledKeyword").toString()
					+ QString("\"");
			ShowMerchantList(pane, title);
			break;
		/////////////////////////////////////////////////////////////
		}

	}
	else {
		ExceptionHandler::ShowError(ERROR_INVALID_RESPONSE);
	}

	// Cleanup
	reply->deleteLater();
}

void QmlEventHandler::ShowMerchantDetails(NavigationPane * pane,
		bool hasSlider) {

	// Prepare Ui object

	QmlDocument *qml = QmlDocument::create("asset:///DetailPage.qml").parent(this);

	qml->setContextProperty("eventHandler", this);

	Page *page = qml->createRootObject<Page>();

	QVariantMap map = m_QVL.at(0).toMap(), venueMap, photoMap;

	QVariant val = map["venue"];
	QString merchantName="", merchantArea="";

	if (!val.isValid()) {
		if (map.count() > 0 && map.contains("name")) {
			venueMap = map;
		} else {
			qDebug() << "+ Invalid Venue info";
			ExceptionHandler::ShowError(ERROR_VENUE_INVALID);
			return;
		}
	} else
		venueMap = val.value<QVariantMap>();

	// set the loaded data.
	QVariant tmp = venueMap["name"];
	page->setProperty("merchantNameTxt", tmp.toString());
	if (m_pCachedContents->contains("merchantName"))
		m_pCachedContents->remove("merchantName");
	m_pCachedContents->insert("merchantName", tmp);
	merchantName = tmp.toString();

	tmp = venueMap["address"];
	page->setProperty("merchantAddressTxt", tmp.toString());

	QString str;
	tmp = venueMap["tel"];
	str = tmp.isValid() ? tmp.toString() : "";
	QString label =
			QString(
					"<span style='color:black;'><div><span style='font-size:x-large; font-style:bold'>PHONE</span></div><div>")
					+ str + QString("</div></span>");
	page->setProperty("phoneNumberTxt", label);
	page->setProperty("phoneNum", tmp.toString());

	tmp = venueMap["web"];
	str = tmp.isValid() ? tmp.toString() : "";
	str = str.replace("?", "/", Qt::CaseInsensitive);
	str = str.replace("&", "/", Qt::CaseInsensitive);
	str = str.length() > 25 ? str.left(25) + QString("...") : str;

	label =
			QString(
					"<span style='color:black;'><div><span style='font-size:x-large; font-style:bold'>WEB</span></div><div>")
					+ str + QString("</div></span>");
	page->setProperty("urlText", label);

	InvokeQuery query;
	query.setUri(tmp.toString());
	Invocation* pInvocation(Invocation::create(&query));
	qml->setContextProperty("invoker", pInvocation);

	tmp = venueMap["latitude"];
	page->setProperty("merchantLatitude", tmp.toString());

	tmp = venueMap["longitude"];
	page->setProperty("merchantLongitude", tmp.toString());

	photoMap = venueMap["area"].toMap();
	merchantArea = photoMap["name"].toString();

	if(merchantArea == ""){
		if (m_pCachedContents->contains("areaList")) {
			QVariantList qvlTmp = m_pCachedContents->value("areaList").toList();

			for(int i = 0; i < qvlTmp.count(); i++) {
				int merchantId = qvlTmp[i].toMap().value("id").toString().toInt();
				if(merchantId == venueMap["area_id"].toString().toInt()){
					merchantArea = qvlTmp[i].toMap().value("name").toString();
					break;
				}
			}

		}
	}
	page->setProperty("highlightTxt", merchantName + QString(" - ") + merchantArea);

	// review
	tmp = venueMap["review"];
//    page->setProperty("reviewTxt", tmp.toString());
	if (m_pCachedContents->contains("merchantReview"))
		m_pCachedContents->remove("merchantReview");
	m_pCachedContents->insert("merchantReview", tmp);

	// photos
	val = venueMap["photos"];

	if (!val.isValid()) {
		qDebug() << "+ Invalid photo list";
		ExceptionHandler::ShowError(ERROR_PHOTOLIST_INVALID);
	} else {
		QVariantList photoList = val.value<QVariantList>();
		if (hasSlider && photoList.length() > 1) {
			QmlDocument *qmlSlider = QmlDocument::create(
					"asset:///ImageSlider.qml").parent(this);
			Container *slider = qmlSlider->createRootObject<Container>();
			WebImageView *pImage;

			for (int i = 0; i < photoList.length(); i++) {
				photoMap = photoList.at(i).value<QVariantMap>();

				tmp = photoMap["tn_240x160"];

				// create image slider
				pImage = new WebImageView();
				pImage->setEventHandler(this);
				pImage->setUrl(tmp.toString());
				pImage->setClickable(true);

				tmp = photoMap["tn_640x425"];
				pImage->setLargeImageUrl(tmp.toString());

				slider->add(pImage);

			}

			ScrollView * scrollView = page->findChild<ScrollView*>(
					"imageScrollView");
			scrollView->setContent(slider);
		}

		// logo image
		photoMap = photoList.at(0).value<QVariantMap>();
		tmp = photoMap["tn_640x425"];
		page->setProperty("logoUrl", tmp.toString());
		m_pCachedContents->insert("merchantLogoUrl", tmp);
	}

	// show the ui
	pane->push(page);

	unlock();
}

void QmlEventHandler::OnTouchImageSlider(WebImageView* pImageView) {
	if (m_lock)
		return;
	lock();

	QUrl url = pImageView->largeImageUrl();

	QmlDocument * qml = QmlDocument::create("asset:///ImageView.qml").parent(
			this);
	Page *page = qml->createRootObject<Page>();

	//Container * newContainer = page->findChild<Container*>("imageView");//by KJB
	WebImageView * image = new WebImageView();
	image->setUrl(url);

	page->setProperty("logoUrl", url);

	QString paneName;
	if (m_pCachedContents->contains("curNavPane"))
		paneName = m_pCachedContents->value("curNavPane").toString();

	if (paneName == "venues")
		m_pMainInstance->getVenuesNavPane()->push(page);
	else if (paneName == "search")
		m_pMainInstance->getSearchNavPane()->push(page);
	else if (paneName == "promo")
		m_pMainInstance->getPromoNavPane()->push(page);
	else if (paneName == "aroundme")
		m_pMainInstance->getAroundmeNavPane()->push(page);
	else //if (paneName == "home")
		m_pMainInstance->getHomeNavPane()->push(page);

	unlock();
}

void QmlEventHandler::onTouchReview() {
	if (m_lock)
		return;
	lock();

	QmlDocument * qml = QmlDocument::create("asset:///Review.qml").parent(this);
	Page *page = qml->createRootObject<Page>();

//    page->setProperty("logoUrl", logoUrl);
	QVariant tmp = m_pCachedContents->value("merchantName");
	page->setProperty("title", tmp.toString());

	tmp = m_pCachedContents->value("merchantReview");
	page->setProperty("review", tmp.toString());

	tmp = m_pCachedContents->value("merchantLogoUrl");
	page->setProperty("logoUrl", tmp.toString());

	QString paneName;
	if (m_pCachedContents->contains("curNavPane"))
		paneName = m_pCachedContents->value("curNavPane").toString();

	if (paneName == "venues")
		m_pMainInstance->getVenuesNavPane()->push(page);
	else if (paneName == "search")
		m_pMainInstance->getSearchNavPane()->push(page);
	else if (paneName == "aroundme")
		m_pMainInstance->getAroundmeNavPane()->push(page);
	else
		m_pMainInstance->getHomeNavPane()->push(page);

	unlock();
}

void QmlEventHandler::onTouchVenuesTab(QObject* venuesListContainer) {
	if (m_lock)
		return;
	lock(venuesListContainer);

	if(m_pJSONConnector->RequestAreaData()) {
		m_RequestType = REQUEST_AREAS;
	}
	else {
		ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
		unlock();
	}

}

void QmlEventHandler::onTouchPromoTab(QObject* promoIndicator) {
	if (m_lock)
		return;
	lock(promoIndicator);

	if(m_pJSONConnector->RequestPromosData())
		m_RequestType = REQUEST_PROMOS;
	else {
		ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
		unlock();
	}
}

void QmlEventHandler::onTouchSearchTab() {
	if (m_lock)
		return;
	lock();

	if(m_pJSONConnector->RequestAreaData())
		m_RequestType = REQUEST_SEARCH_LOCATION;
	else {
		ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
		unlock();
	}
}

void QmlEventHandler::UpdateVenuesList() {
	GroupDataModel *areaModel = new GroupDataModel();
	areaModel->setParent(this);
	areaModel->insertList(m_QVL);
	areaModel->setGrouping(ItemGrouping::None);
	QStringList li;
	li << "name";
	areaModel->setSortingKeys(li);
	areaModel->setSortedAscending(true);

	NavigationPane *navVenues = m_pMainInstance->getVenuesNavPane();

	// Get the ListView from QML and setup a DataModel from the JSON data.
	// Connect to the stampList triggered signal to the onListTriggered Slot function.
	ListView* pList = navVenues->findChild<ListView*>("areaList");
	pList->setDataModel(areaModel);

	unlock();
}

void QmlEventHandler::UpdateLocationList(NavigationPane * pane) {
	QVariantMap qvm;
	//NavigationPane *navVenues = m_pMainInstance->getVenuesNavPane();

	// Get the ListView from QML and setup a DataModel from the JSON data.
	// Connect to the stampList triggered signal to the onListTriggered Slot function.
	DropDown* pDropList = pane->findChild<DropDown*>("locationDropBox");

	pDropList->removeAll();

	for(int i = 0; i < m_QVL.count(); i++) {
		qvm = m_QVL.at(i).toMap();

		Option* nop = new Option(pDropList);
		nop->setText(qvm.value("name").toString());
		nop->setValue(qvm.value("id").toString());

		pDropList->add(nop);
	}
	unlock();
}

void QmlEventHandler::UpdatePromoList() {
	AbstractPane* page = m_pMainInstance->getAbstractPane();

	QVariantMap vMap, tmpMap;
//    for(int i=0; i<m_QVL.count(); i++){
//    }

	if (m_pCachedContents->contains("promolist"))
		m_pCachedContents->remove("promolist");

	m_pCachedContents->insert("promolist", m_QVL);

	// Promo 1
	vMap = m_QVL.at(0).toMap();

	QVariant var1 = vMap.value("title"), var2 = vMap.value("summary");
	QString text =
			"<span><div><span style='font-size:x-large; font-style:bold'>"
					+ var1.toString() + "</span></div><div>" + var2.toString()
					+ "</div></span>";
	page->setProperty("promo1Title", text);

	QVariantList photoList = vMap.value("photos").value<QVariantList>();
	tmpMap = photoList.at(0).toMap();
	var1 = tmpMap.value("tn_240x160");
	page->setProperty("promo1Logo", var1);

	// Promo 2
	vMap = m_QVL.at(1).toMap();

	var1 = vMap.value("title");
	var2 = vMap.value("summary");
	text = "<span><div><span style='font-size:x-large; font-style:bold'>"
			+ var1.toString() + "</span></div><div>" + var2.toString()
			+ "</div></span>";
	page->setProperty("promo2Title", text);

	photoList = vMap.value("photos").value<QVariantList>();
	tmpMap = photoList.at(0).toMap();
	var1 = tmpMap.value("tn_240x160");
	page->setProperty("promo2Logo", var1);

	unlock();
}

void QmlEventHandler::onTouchAreaItem(QObject* indicator, QObject* labelAreaId,
		QObject* labelAreaName) {
	if (m_lock)
		return;
	lock(indicator);

	QString var = ((Label*) labelAreaId)->text();
	if (m_pCachedContents->contains("seledKeyword"))
		m_pCachedContents->remove("seledKeyword");

	m_pCachedContents->insert("seledKeyword", ((Label*) labelAreaName)->text());

	if(m_pJSONConnector->RequestAreaIdData(var))
		m_RequestType = REQUEST_AREA_ID;
	else {
		ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
		unlock();
	}
}

void QmlEventHandler::onSelectArea(QObject* searchIndicator, QObject* option) {
	if (m_lock)
		return;
	lock(searchIndicator);

	Option* pOpt = (Option*)option;

	QString var = pOpt->value().toString();

	if (m_pCachedContents->contains("seledKeyword"))
		m_pCachedContents->remove("seledKeyword");
	m_pCachedContents->insert("seledKeyword", pOpt->text());

	if(m_pJSONConnector->RequestAreaIdData(var))
			m_RequestType = REQUEST_SELECT_LOCATION;
	else {
		ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
		unlock();
	}
}

void QmlEventHandler::ShowMerchantList(NavigationPane * pane, QString title) {

	GroupDataModel *areaModel = new GroupDataModel();
	QVariantMap itemMap, tmpMap, tmpMap1;
	QVariantList itemList;
	QString name;
	itemList.clear();

	for (int i = 0; i < m_QVL.count(); i++) {
		tmpMap = m_QVL.at(i).toMap();

		name = tmpMap.value("name").toString();

		itemMap.insert("name", tmpMap.value("name"));
		tmpMap1 = tmpMap.value("photos").toList().at(0).toMap();

		if (tmpMap1.contains("tn_160x100"))
			itemMap.insert("photos", tmpMap1.value("tn_160x100"));

//		itemMap.insert("details", tmpMap);
		if (m_pCachedContents->contains(name))
			m_pCachedContents->remove(name);

		m_pCachedContents->insert(name, tmpMap);

		itemList.append(itemMap);

		itemMap.clear();
	}

	areaModel->insertList(itemList);

	areaModel->setParent(this);
	areaModel->setGrouping(ItemGrouping::None);
	QStringList li;
	li << "name";
	areaModel->setSortingKeys(li);
	areaModel->setSortedAscending(true);

	QmlDocument * qml =
			QmlDocument::create("asset:///VenueDetailPage.qml").parent(this);
	qml->setContextProperty("eventHandler", this);
	Page *page = qml->createRootObject<Page>();

	page->setProperty("title", title);

	// Get the ListView from QML and setup a DataModel from the JSON data.
	// Connect to the stampList triggered signal to the onListTriggered Slot function.
	ListView* pList = page->findChild<ListView*>("pointList");
	pList->setDataModel(areaModel);

	pane->push(page);

	unlock();
}

void QmlEventHandler::onTouchMerchantItem(QObject* merchantName) {
	if (m_lock)
		return;
	lock();

	Label *pNameLabel = (Label*) merchantName;

	if (m_pCachedContents->contains(pNameLabel->text())) {

		m_QVL.clear();
		m_QVL.insert(0, m_pCachedContents->value(pNameLabel->text()));

		QString paneName = "";
		if (m_pCachedContents->contains("curNavPane"))
			paneName = m_pCachedContents->value("curNavPane").toString();

		if (paneName == "venues")
			ShowMerchantDetails(m_pMainInstance->getVenuesNavPane(), true);
		else
			ShowMerchantDetails(m_pMainInstance->getSearchNavPane(), true);
	} else {
		qDebug() << "+ Invalid Venue info";
		ExceptionHandler::ShowError(ERROR_MERCHANT_INVALID);
	}

	unlock();
}

void QmlEventHandler::onSearch(QObject* indicator, QObject* item) {
	if(m_lock)
		return;
	lock(indicator);

	TextField* pField = (TextField*) item;

	if(pField->text().isEmpty()) {
		ExceptionHandler::ShowError(ERROR_SEARCHKEYWORD_EMPTY);
		unlock();
		return;
	}

	if (m_pCachedContents->contains("seledKeyword"))
		m_pCachedContents->remove("seledKeyword");
	m_pCachedContents->insert("seledKeyword", pField->text());

	if(m_pJSONConnector->RequestSearchData(pField->text()))
			m_RequestType = REQUEST_SEARCH;
	else {
		ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
		unlock();
	}

}

void QmlEventHandler::onTouchPromo(QObject* label) {
	Label* promoNumLabel = (Label*) label;
	int num = promoNumLabel->text().toInt();
	NavigationPane *navPromo = m_pMainInstance->getPromoNavPane();

	if (m_pCachedContents->contains("promolist")) {
		QmlDocument * qml =
				QmlDocument::create("asset:///PromoDetail.qml").parent(this);
		qml->setContextProperty("eventHandler", this);
		Page *page = qml->createRootObject<Page>();

		QVariantList qvl = m_pCachedContents->value("promolist").value<
				QVariantList>();
		QVariantMap vMap = qvl.at(num - 1).toMap(), tmpMap;

		QVariant var = vMap.value("title");
		page->setProperty("title", var);

		var = vMap.value("description");
		page->setProperty("desc", var);

		QVariantList photoList = vMap.value("photos").value<QVariantList>();
		tmpMap = photoList.at(0).toMap();
		var = tmpMap.value("tn_640x425");
		page->setProperty("logoUrl", var);

		// photos
		QVariant val = vMap["photos"];

		if (!val.isValid()) {
			qDebug() << "+ Invalid photo list";
			ExceptionHandler::ShowError(ERROR_PHOTOLIST_INVALID);
		} else {
			QVariantList photoList = val.value<QVariantList>();

			QmlDocument *qmlSlider = QmlDocument::create(
					"asset:///ImageSlider.qml").parent(this);
			Container *slider = qmlSlider->createRootObject<Container>();
			WebImageView *pImage;

			for (int i = 0; i < photoList.length(); i++) {
				vMap = photoList.at(i).value<QVariantMap>();

				var = vMap["tn_240x160"];

				// create image slider
				pImage = new WebImageView();
				pImage->setEventHandler(this);
				pImage->setUrl(var.toString());
				pImage->setClickable(true);

				var = vMap["tn_640x425"];
				pImage->setLargeImageUrl(var.toString());

				slider->add(pImage);

			}

			ScrollView * scrollView = page->findChild<ScrollView*>(
					"imageScrollView");
			scrollView->setContent(slider);

		}

		navPromo->push(page);
	} else {

	}
}

void QmlEventHandler::UpdateHomeList() {
	AbstractPane* page = m_pMainInstance->getAbstractPane();

	QVariantMap map = m_pCachedContents->value("BarOfMonth").toList().at(0).toMap(), venueMap, photoMap;

	QVariant val = map["venue"];

	if (!val.isValid()) {
		if (map.count() > 0 && map.contains("name")) {
			venueMap = map;
		} else {
			qDebug() << "+ Invalid Venue info";
			ExceptionHandler::ShowError(ERROR_VENUE_INVALID);
			return;
		}
	} else
		venueMap = val.value<QVariantMap>();

	val = venueMap["photos"];


	page->setProperty("logoBarOfMonthURL", val.toList().at(0).toMap().value("tn_640x425").toString());

	map = m_pCachedContents->value("ClubOfMonth").toList().at(0).toMap();

	val = map["venue"];

	if (!val.isValid()) {
		if (map.count() > 0 && map.contains("name")) {
			venueMap = map;
		} else {
			qDebug() << "+ Invalid Venue info";
			ExceptionHandler::ShowError(ERROR_VENUE_INVALID);
			return;
		}
	} else
		venueMap = val.value<QVariantMap>();

	val = venueMap["photos"];

	page->setProperty("logoClubOfMonthURL", val.toList().at(0).toMap().value("tn_640x425").toString());

	m_pMainInstance->ShowBootup();

	unlock();
}

void QmlEventHandler::ShowHome() {
	m_pMainInstance->ShowHome();
}

////////////////////////////////////////////////////////////////////
/////////////////////AroundMe by KJB////////////////////////////////
void QmlEventHandler::onTouchAroundmeTab(QObject* indicator)
{
	if (m_lock)
	return;
	lock(indicator);

	emit InitializeAroundMe();
}

QVariantList QmlEventHandler::worldToPixelInvokable(QObject* mapObject, double latitude, double longitude) const
{
	Container* mapContainer = qobject_cast<Container*>(mapObject);
	MapView* mapview = (MapView*)(mapContainer->at(0));
    const Point worldCoordinates = Point(latitude, longitude);
    const QPoint pixels = mapview->worldToWindow(worldCoordinates);

    return QVariantList() << pixels.x() << pixels.y();
}

void QmlEventHandler::updateMarkers(QObject* mapObject, QObject* containerObject) const
{
	Container* mapContainer = qobject_cast<Container*>(mapObject);
	MapView* mapview = (MapView*)(mapContainer->at(0));
    Container* container = qobject_cast<Container*>(containerObject);

    for (int i = 0; i < container->count(); i++) {
        const QPoint xy = worldToPixel(mapview,
        			container->at(i)->property("lat").value<double>(),
        			container->at(i)->property("lon").value<double>());
        container->at(i)->setProperty("x", xy.x());
        container->at(i)->setProperty("y", xy.y());

        qDebug()<<i<<":  xpos: "<<xy.x()<<"   ypos: "<<xy.y();
    }
}

QPoint QmlEventHandler::worldToPixel(QObject* mapObject, double latitude, double longitude) const
{
    MapView* mapview = qobject_cast<MapView*>(mapObject);
    const Point worldCoordinates = Point(latitude, longitude);

    return mapview->worldToWindow(worldCoordinates);
}

//This function is of updating venue data placed within specified range around me.
//This is called whenever initialization and changing of range from QML.
//range: changed range
//pList: pointer to ListView showing venues with list.
//pContainer: pointer to Container showing venues with pin of Map.
void QmlEventHandler::onChangeAroundMe(QObject* indicator, double lat, double logu, double radius)
{
	unlock();//for initialize
	lock(indicator);

	//radius *= 10;//----------------------//////////////////////////
    //1. Request getting venue data within changed range around me.
    QString strLat = QString::number(lat);
    QString strLogu = QString::number(logu);
    QString strRads = QString::number(radius);
    qDebug()<<"====latitude: "<<strLat<<" longitude: "<<strLogu<<" radius: "<<strRads<<"====";

	if(m_pJSONConnector->RequestNearByData(strLat, strLogu, strRads))
		m_RequestType = REQUEST_AROUNDME;
	else {
		ExceptionHandler::ShowError(ERROR_NETWORK_INVALID);
		unlock();
	}
}

//Finish SIGNAL Function
void QmlEventHandler::UpdateAroundMeList()
{
	qDebug()<<"====UpdateAroundMeList====";
//#define FILEDEBUG
#ifdef FILEDEBUG
	JsonDataAccess jsondata;
	static int iiii = 0;
	if(iiii++ % 3 == 2){
	m_QVL = jsondata.load(QDir::currentPath() + "/app/native/assets/models/aroundme.json").value<QVariantList>();
	}
#endif
	GroupDataModel *pinsModel = new GroupDataModel();
	QVariantMap itemMap, tmpMap, tmpMap1;
	QVariantList itemList;
	QString name;
	itemList.clear();
	pinsModel->setParent(this);

	//1. Create GroupDataModel with referring to json result.
	// and request for pinContainer of MapView to add pin.
	// first of all remove pins in pinContainer.
	emit removePins();

	for (int i = 0; i < m_QVL.count(); i++) {
		qDebug()<<"======"<<i<<"======";
		tmpMap = m_QVL.at(i).toMap();

		itemMap.insert("id", tmpMap.value("area_id"));
		qDebug()<<"====id:"<<tmpMap.value("area_id").toString()<<"====";

		name = tmpMap.value("name").toString();
		itemMap.insert("name", tmpMap.value("name"));
		qDebug()<<"====name:"<<tmpMap.value("name").toString()<<"====";
		itemMap.insert("latitude", tmpMap.value("latitude"));
		qDebug()<<"====latitude:"<<tmpMap.value("latitude").toString()<<"====";
		itemMap.insert("longitude", tmpMap.value("longitude"));
		qDebug()<<"====longitude:"<<tmpMap.value("longitude").toString()<<"====";

		tmpMap1 = tmpMap.value("photos").toList().at(0).toMap();

		if (tmpMap1.contains("tn_300x300"))
			itemMap.insert("tn_300x300", tmpMap1.value("tn_300x300"));
		qDebug()<<"====tn_300x300:"<<tmpMap1.value("tn_300x300").toString()<<"====";

		///request to add pin
		bool ok;
    	emit addPintoContainer(
    			i+1,
    			itemMap.value("id").toInt(&ok),
    			itemMap.value("name").toString(),
    			itemMap.value("latitude").toDouble(&ok),
    			itemMap.value("longitude").toDouble(&ok));
    	qDebug()<<"===="<<i<<"::id:"<<itemMap.value("id").toInt(&ok)<<" name:"<<itemMap.value("name").toString()<<" lat:"<<itemMap.value("latitude").toDouble(&ok)<<" long:"<<itemMap.value("longitude").toDouble(&ok)<<"====";

    	if (m_pCachedContents->contains(name))
			m_pCachedContents->remove(name);

		m_pCachedContents->insert(name, tmpMap);

		itemList.append(itemMap);

		itemMap.clear();
	}
	pinsModel->insertList(itemList);

	//2. Select grouping mode of GroupModel to false.
	pinsModel->setGrouping(ItemGrouping::None);

	//3. Sorting.
	QStringList li;
	li << "name";
	pinsModel->setSortingKeys(li);
	pinsModel->setSortedAscending(true);

	//4. Set created GroupDataModel as datamodel of ListView.
	ListView* aroundmeList = m_pMainInstance->getAbstractPane()->findChild<ListView*>("pointList");
	aroundmeList->setDataModel(pinsModel);

	if(m_QVL.count() == 0) emit showErrorLabel();

	unlock();
}

void QmlEventHandler::onTouchAroundMeItem (QObject* indicator, QString venueName)
{
	if (m_lock)
	return;
	lock(indicator);

	if (m_pCachedContents->contains(venueName)) {

		m_QVL.clear();
		m_QVL.insert(0, m_pCachedContents->value(venueName));

		QString paneName = "";
		if (m_pCachedContents->contains("curNavPane"))
			paneName = m_pCachedContents->value("curNavPane").toString();
		else
			m_pCachedContents->insert("curNavPane", "aroundme");

		ShowMerchantDetails(m_pMainInstance->getAroundmeNavPane(), true);
	} else {
		qDebug() << "+ Invalid AroundMe info";
		ExceptionHandler::ShowError(ERROR_MERCHANT_INVALID);
	}

	unlock();

}

void QmlEventHandler::Sleep(int msec, int sender)
{
	switch(sender){
	case 0:
		QTimer::singleShot(msec, this, SLOT(ShowHomeAuto()));
		break;
	case 3:
		QTimer::singleShot(msec, this, SLOT(ShowMapView()));
		break;
	}
}

void QmlEventHandler::ShowMapView()
{
	emit ShowenMapView();
}

void QmlEventHandler::ShowHomeAuto()
{
	this->ShowHome();
}

void QmlEventHandler::startCountDown()
{
	SystemDialog *pDiag = new SystemDialog("Ok");
	pDiag->setTitle("The Guide by Juice");
	pDiag->setBody("The result is empty.\n\rPlease choose another one.");

	pDiag->exec();

}


//////////////////////////////////////////////////////////////////////////
