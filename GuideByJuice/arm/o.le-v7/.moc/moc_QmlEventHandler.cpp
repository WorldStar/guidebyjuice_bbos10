/****************************************************************************
** Meta object code from reading C++ file 'QmlEventHandler.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/QmlEventHandler.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QmlEventHandler.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QmlEventHandler[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      30,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      27,   17,   16,   16, 0x05,
      52,   16,   16,   16, 0x05,
      73,   16,   16,   16, 0x05,
     110,   89,   16,   16, 0x05,
     159,   16,   16,   16, 0x05,
     172,   16,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
     189,   16,   16,   16, 0x0a,
     210,  204,   16,   16, 0x0a,
     242,   16,   16,   16, 0x0a,

 // methods: signature, parameters, type, tag, flags
     256,   16,   16,   16, 0x02,
     283,   16,   16,   16, 0x02,
     309,   16,   16,   16, 0x02,
     328,   16,   16,   16, 0x02,
     339,   16,   16,   16, 0x02,
     356,   16,   16,   16, 0x02,
     376,   16,   16,   16, 0x02,
     400,  397,   16,   16, 0x02,
     444,   16,   16,   16, 0x02,
     476,  474,   16,   16, 0x02,
     504,  474,   16,   16, 0x02,
     536,   16,   16,   16, 0x02,
     559,   16,   16,   16, 0x02,
     593,   16,   16,   16, 0x02,
     619,  609,   16,   16, 0x02,
     690,  661,  648,   16, 0x02,
     762,  736,   16,   16, 0x02,
     821,  795,   16,   16, 0x02,
     889,  869,   16,   16, 0x02,
     936,  927,   16,   16, 0x02,
     951,   16,   16,   16, 0x02,

       0        // eod
};

static const char qt_meta_stringdata_QmlEventHandler[] = {
    "QmlEventHandler\0\0dataGroup\0"
    "UpdateAroundme(QObject*)\0InitializeAroundMe()\0"
    "ShowenMapView()\0i,idnum,name,lat,lon\0"
    "addPintoContainer(int,int,QString,double,double)\0"
    "removePins()\0showErrorLabel()\0"
    "ShowHomeAuto()\0reply\0"
    "requestFinished(QNetworkReply*)\0"
    "ShowMapView()\0onTouchVenuesTab(QObject*)\0"
    "onTouchPromoTab(QObject*)\0onTouchSearchTab()\0"
    "ShowHome()\0startCountDown()\0"
    "onTouchBarOfMonth()\0onTouchClubOfMonth()\0"
    ",,\0onTouchAreaItem(QObject*,QObject*,QObject*)\0"
    "onTouchMerchantItem(QObject*)\0,\0"
    "onSearch(QObject*,QObject*)\0"
    "onSelectArea(QObject*,QObject*)\0"
    "onTouchPromo(QObject*)\0"
    "OnTouchImageSlider(WebImageView*)\0"
    "onTouchReview()\0indicator\0"
    "onTouchAroundmeTab(QObject*)\0QVariantList\0"
    "mapObject,latitude,longitude\0"
    "worldToPixelInvokable(QObject*,double,double)\0"
    "mapObject,containerObject\0"
    "updateMarkers(QObject*,QObject*)\0"
    "indicator,lat,logu,radius\0"
    "onChangeAroundMe(QObject*,double,double,double)\0"
    "indicator,venueName\0"
    "onTouchAroundMeItem(QObject*,QString)\0"
    "i,sender\0Sleep(int,int)\0unlock()\0"
};

void QmlEventHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QmlEventHandler *_t = static_cast<QmlEventHandler *>(_o);
        switch (_id) {
        case 0: _t->UpdateAroundme((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 1: _t->InitializeAroundMe(); break;
        case 2: _t->ShowenMapView(); break;
        case 3: _t->addPintoContainer((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< double(*)>(_a[5]))); break;
        case 4: _t->removePins(); break;
        case 5: _t->showErrorLabel(); break;
        case 6: _t->ShowHomeAuto(); break;
        case 7: _t->requestFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 8: _t->ShowMapView(); break;
        case 9: _t->onTouchVenuesTab((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 10: _t->onTouchPromoTab((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 11: _t->onTouchSearchTab(); break;
        case 12: _t->ShowHome(); break;
        case 13: _t->startCountDown(); break;
        case 14: _t->onTouchBarOfMonth(); break;
        case 15: _t->onTouchClubOfMonth(); break;
        case 16: _t->onTouchAreaItem((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2])),(*reinterpret_cast< QObject*(*)>(_a[3]))); break;
        case 17: _t->onTouchMerchantItem((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 18: _t->onSearch((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 19: _t->onSelectArea((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 20: _t->onTouchPromo((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 21: _t->OnTouchImageSlider((*reinterpret_cast< WebImageView*(*)>(_a[1]))); break;
        case 22: _t->onTouchReview(); break;
        case 23: _t->onTouchAroundmeTab((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 24: { QVariantList _r = _t->worldToPixelInvokable((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QVariantList*>(_a[0]) = _r; }  break;
        case 25: _t->updateMarkers((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 26: _t->onChangeAroundMe((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4]))); break;
        case 27: _t->onTouchAroundMeItem((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 28: _t->Sleep((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 29: _t->unlock(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QmlEventHandler::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QmlEventHandler::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QmlEventHandler,
      qt_meta_data_QmlEventHandler, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QmlEventHandler::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QmlEventHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QmlEventHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QmlEventHandler))
        return static_cast<void*>(const_cast< QmlEventHandler*>(this));
    return QObject::qt_metacast(_clname);
}

int QmlEventHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    }
    return _id;
}

// SIGNAL 0
void QmlEventHandler::UpdateAroundme(QObject * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QmlEventHandler::InitializeAroundMe()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void QmlEventHandler::ShowenMapView()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void QmlEventHandler::addPintoContainer(int _t1, int _t2, QString _t3, double _t4, double _t5)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QmlEventHandler::removePins()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void QmlEventHandler::showErrorLabel()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}
QT_END_MOC_NAMESPACE
