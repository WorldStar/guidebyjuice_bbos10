import bb.cascades 1.0
import org.labsquare 1.0 // WebImageView
import bb.system.phone 1.0
import bb.data 1.0
import bb.platform 1.0
import my.lib 1.0 // QmlEventHandler

Page {
    property alias merchantNameTxt: merchantName.text
    property alias merchantAddressTxt: merchantAddress.text
    property alias phoneNumberTxt: phoneNumber.text
    property alias highlightTxt: highlight.text

    property alias merchantLatitude: routeInvoker.endLatitude
    property alias merchantLongitude: routeInvoker.endLongitude
    property alias urlText: merchantWeb.text
    property alias imageScrollContent: imageScrollView.content

    property NavigationPane nav
    property alias logoUrl: logoImage.url
    property alias phoneNum: phonenum.text
    property alias reviewTxt: reviewLabel.text

    Container {
        id: mainContainer

        attachedObjects: [
            ComponentDefinition {
                id: review
                source: "Review.qml"
            },
            Label {
                id: reviewLabel

            },
            ImagePaintDefinition {
                id: backgroundPage
                imageSource: "asset:///images/bg.png"
            },
            ImagePaintDefinition {
                id: titleBar
                imageSource: "asset:///images/bar_top.png"
            },
            ImagePaintDefinition {
                id: logoHightlight
                imageSource: "asset:///images/bar_pink_trans.png"

            }

        ]
        background: backgroundPage.imagePaint
        // title bar
        Container {
            background: titleBar.imagePaint
            layout: DockLayout {
            }
            preferredWidth: 768.0
            preferredHeight: 136.0
            leftPadding: 20.0
            topPadding: 20.0
            layoutProperties: AbsoluteLayoutProperties {

            }
            Label {
                id: title
                verticalAlignment: VerticalAlignment.Top
                horizontalAlignment: HorizontalAlignment.Left
                text: "Details"
                textStyle.fontSize: FontSize.XLarge
                textStyle.fontWeight: FontWeight.Bold
            }
        }
        ScrollView {
            id: scrollViewContainer
            layoutProperties: AbsoluteLayoutProperties {
                positionY: 112.0

            }
            scrollViewProperties {
                scrollMode: ScrollMode.Vertical
            }
            Container {
                id: grayContainer
                background: Color.Black

                // merchant logo
                Container {
                    id: logoContainer
                    layout: DockLayout {

                    }
                    WebImageView {
                        id: logoImage
                        //                        url: "http://iphone.catchacorp.com/media/cache/c6/9e/c69e532f8c550aaaac76b74375dd34da.jpg"
                        scalingMethod: ScalingMethod.AspectFit
                        preferredHeight: 500.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                    Container {
                        background: logoHightlight.imagePaint
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Bottom
                        leftPadding: 30.0
                        preferredHeight: 70.0
                        opacity: 0.9
                        preferredWidth: 900.0
                        Label {
                            id:highlight
                            text: "Damansara Height"
                            textStyle.color: Color.White
                            textStyle.fontSize: FontSize.Large
                            verticalAlignment: VerticalAlignment.Center
                        }
                    }
                    ActivityIndicator {
                        id: imageloading
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        scaleX: 2.0
                        scaleY: 2.0
                    }
                    onCreationCompleted: {
                        imageloading.start();
                        logoImage.imageDownloaded.connect(onImageDownloaded);
                    }
                    function onImageDownloaded() {
                        imageloading.stop();
                    }
                }

                // gray back container
                Container {
                    id: detailContainer
                    background: Color.create("#fff0f0f0")

                    leftPadding: 25.0
                    rightPadding: 25.0
                    bottomPadding: 10.0
                    // merchant detail
                    Label {
                        id: merchantName
                        //textStyle.fontWeight: FontWeight.Bold
                        textStyle.fontSize: FontSize.XXLarge

                    }
                    Label {
                        id: merchantAddress
                        multiline: true
                        textStyle.fontSize: FontSize.Large

                    }
                    // review
                    Container {
                        background: Color.White
                        layout: DockLayout {
                        }
                        preferredWidth: 760.0
                        preferredHeight: 143.0
                        Label {
                            id: merchantReview
                            multiline: true
                            text: "<span style='color:black;'><div><span style='font-size:x-large; font-style:bold'>" + "REVIEW" + "</span></div><div>" + "Impressions of this venue" + "</div></span>"
                            textFormat: TextFormat.Html
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Left
                            leftMargin: 10.0
                        }
                        ImageView {
                            imageSource: "asset:///images/btn_blk_review.png"
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Right
                        }
                        onTouch: {
                            if (event.isUp()) eventHandler.onTouchReview();
                        }
                        leftPadding: 10.0
                    }
                    // phone
                    Container {
                        id: phoneContainer
                        background: Color.White
                        layout: DockLayout {
                        }
                        preferredWidth: 760.0
                        preferredHeight: 143.0
                        Label {
                            id: phoneNumber
                            multiline: true
                            text: "<span style='color:black;'><div><span style='font-size:x-large; font-style:bold'>PHONE</span></div><div>%PHONE%</div></span>"
                            textFormat: TextFormat.Html
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Left
                            leftMargin: 10.0
                        }
                        ImageView {
                            imageSource: "asset:///images/btn_blk_call.png"
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Right
                        }
                        onTouch: {
                            if (event.isUp()) {
                                //                                phone.initiateCellularCall("20137545");
                                phone.requestDialpad(phoneNum);
                            }
                            //phone.requestDialpad(dataModel.data(indexPath).Phone)
                        }
                        topMargin: 5.0

                        attachedObjects: [
                            Phone {
                                id: phone
                            },
                            Label {
                                id: phonenum
                                visible: false

                            }
                        ]
                        leftPadding: 10.0
                    }

                    // Web
                    Container {
                        background: Color.White
                        layout: DockLayout {
                        }
                        preferredWidth: 760.0
                        preferredHeight: 143.0
                        Label {
                            id: merchantWeb
                            multiline: true
                            text: "<span style='color:black;'><div><span style='font-size:x-large; font-style:bold'>WEB</span></div><div>%URL%</div></span>"
                            textFormat: TextFormat.Html
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Left
                            leftMargin: 10.0
                        }
                        ImageView {
                            imageSource: "asset:///images/btn_blk_web.png"
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Right
                        }
                        topMargin: 5.0
                        onTouch: {
                            if (event.isUp()) {
                                //diag.open();
                                invoker.trigger("bb.action.OPEN");
                            }
                        }
                        leftPadding: 10.0
                    }
                    // Navigation
                    Container {
                        background: Color.White
                        layout: DockLayout {
                        }
                        preferredWidth: 760.0
                        preferredHeight: 143.0
                        Label {
                            id: merchantNavi
                            multiline: true
                            text: "<span style='color:black;'><div><span style='font-size:x-large; font-style:bold'>TAKE ME THERE</span></div></span>"
                            textFormat: TextFormat.Html
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Left
                            leftMargin: 10.0
                        }
                        ImageView {
                            imageSource: "asset:///images/btn_blk_there-me.png"
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Right
                        }
                        onTouch: {
                            if (event.isUp()) routeInvoker.go();
                        }
                        topMargin: 5.0
                        attachedObjects: [
                            RouteMapInvoker {
                                id: routeInvoker

                            }
                        ]
                        leftPadding: 10.0
                    }
                    // BBM
                    Container {
                        background: Color.White
                        layout: DockLayout {
                        }
                        preferredWidth: 760.0
                        preferredHeight: 143.0
                        Label {
                            id: merchantBBM
                            multiline: true
                            text: "<span style='color:black;'><div><span style='font-size:x-large; font-style:bold'>BLACKBERRY<br />MESSENGER</span></div></span>"
                            textFormat: TextFormat.Html
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Left
                            leftMargin: 10.0
                        }
                        ImageView {
                            imageSource: "asset:///images/btn_blk_bbm.png"
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Right
                        }
                        topMargin: 5.0
                        onTouch: {
                            if (event.isUp()) {
                                //diag.open();
                                bbmInvoker.trigger("bb.action.SHARE");
                            }
                        }
                        attachedObjects: [
                            Invocation {
                                id: bbmInvoker
                                query {
                                    mimeType: "text/plain"
                                }
                            }

                        ]
                        leftPadding: 10.0
                    }
                    // Tweet
                    Container {
                        background: Color.White
                        layout: DockLayout {
                        }
                        preferredWidth: 760.0
                        preferredHeight: 143.0
                        Label {
                            id: merchantTweet
                            multiline: true
                            text: "<span style='color:black;'><div><span style='font-size:x-large; font-style:bold'>TWEET IT</span></div></span>"
                            textFormat: TextFormat.Html
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Left
                            leftMargin: 10.0
                        }
                        ImageView {
                            imageSource: "asset:///images/btn_blk_twitter.png"
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Right
                        }
                        topMargin: 5.0
                        onTouch: {
                            if (event.isUp()) {
                                //diag.open();
                                tweetInvoker.trigger("bb.action.OPEN");
                            }
                        }
                        attachedObjects: [
                            Invocation {
                                id: tweetInvoker
                                query {
                                    mimeType: "text/plain"
                                    uri: "http://mobile.twitter.com"
                                }
                            }

                        ]
                        leftPadding: 10.0
                    }
                    // Facebook
                    Container {
                        background: Color.White
                        layout: DockLayout {
                        }
                        preferredWidth: 760.0
                        preferredHeight: 143.0
                        Label {
                            id: merchantfb
                            multiline: true
                            text: "<span style='color:black;'><div><span style='font-size:x-large; font-style:bold'>SHARE IT <br />ON FACEBOOK</span></div></span>"
                            textFormat: TextFormat.Html
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Left
                            leftMargin: 10.0
                        }
                        ImageView {
                            imageSource: "asset:///images/btn_blk_facebook.png"
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Right
                        }
                        topMargin: 5.0
                        onTouch: {
                            if (event.isUp()) {
                                //diag.open();
                                fbInvoker.trigger("bb.action.OPEN");
                            }
                        }
                        attachedObjects: [
                            Invocation {
                                id: fbInvoker
                                query {
                                    mimeType: "text/plain"
                                    uri: "http://m.facebook.com"
                                }
                            }

                        ]
                        leftPadding: 10.0
                    }

                    // Image slider

                }
                ScrollView {
                    id: imageScrollView
                    objectName: "imageScrollView"
                    scrollViewProperties.scrollMode: ScrollMode.Horizontal
                }
            }
        }
    }
}
