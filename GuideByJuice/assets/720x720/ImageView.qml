import bb.cascades 1.0
import org.labsquare 1.0

Page {
    property alias logoUrl: logoImage.url

    Container {
        background: Color.Black

        preferredHeight: 1280
        layout: DockLayout {

        }
        WebImageView {
            id: logoImage
            scalingMethod: ScalingMethod.AspectFit
            preferredHeight: 0.0
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            preferredWidth: 960.0
        }
        ActivityIndicator {
        	id:imageloading
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            scaleX: 2.0
            scaleY: 2.0
        }
        onCreationCompleted:{
            imageloading.start();
            logoImage.imageDownloaded.connect(onImageDownloaded);
        }
        function onImageDownloaded() {
            imageloading.stop();
        }
    }
}
