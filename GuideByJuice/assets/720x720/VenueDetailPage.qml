import bb.cascades 1.0
import org.labsquare 1.0 // WebImageView
import my.lib 1.0 // QmlEventHandler
import my.QVariantMap 1.0 // QVariantMap

Page {
    //property alias title: titleLabel.text
    
    Container {
        // A paper-style image is used to tile the background.

        attachedObjects: [
            ImagePaintDefinition {
                id: backgroundPage
                imageSource: "asset:///images/bg.png"

            },
            ImagePaintDefinition {
                id: titleBar
                imageSource: "asset:///images/bar_top.png"
            }
        ]

        layout: StackLayout {

        }

        background: backgroundPage.imagePaint
        Container {
            background: titleBar.imagePaint
            layout: DockLayout {
            }
            preferredWidth: 720.0
            preferredHeight: 136.0
            leftPadding: 20.0
            topPadding: 20.0
            Label {
                id: titleLabel
                verticalAlignment: VerticalAlignment.Top
                horizontalAlignment: HorizontalAlignment.Left
                text: "Venues"
                textStyle.fontSize: FontSize.XLarge
                textStyle.fontWeight: FontWeight.Bold
                textStyle.color: Color.Black
            }
        }

        //list container
        Container {

            layout: StackLayout {

            }

            attachedObjects: [
                Label {
                	id: seledMerchantName
                    textStyle.color: Color.Black
                },
                QVariantMap {
                    id: seledMerchantDetail
                    objectName: "seledMerchantDetail"
                }
            ]
            // Main List
            ListView {
                id: pointList
                objectName: "pointList"

                layout: StackListLayout {
                    headerMode: ListHeaderMode.None
                    orientation: LayoutOrientation.TopToBottom
                }

                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        PointItem {

                        }
                    }
                ] // listItemComponents
                horizontalAlignment: HorizontalAlignment.Fill
                
                onTriggered: {
                    // When an item is selected we push the recipe Page in the chosenItem file attribute.
                    var chosenItem = dataModel.data(indexPath);
                    //eventHandler.onTouchAreaItem(areaList);

                    // Create the content page and push it on top to drill down to it.
                    seledMerchantName.text = chosenItem.name;
//                    seledMerchantDetail = chosenItem.details;

                    eventHandler.onTouchMerchantItem(seledMerchantName);
                }
            }

        }
    } // Container
}
