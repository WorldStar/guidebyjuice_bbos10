import bb.cascades 1.0
import org.labsquare 1.0 // WebImageView
import my.lib 1.0 // QmlEventHandler

// Item component for the item list presenting available recipes
Container {
    id: pointContainer

    layout: DockLayout {
    }

    // Item background color.
    // The Item content an image and a text
    Container {
        id: listItem
        horizontalAlignment: HorizontalAlignment.Fill
        preferredWidth: 760
        preferredHeight: 140

        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }

        background: Color.White
        WebImageView {
            preferredWidth: 160
            preferredHeight: 140
            verticalAlignment: VerticalAlignment.Top

            // The image is bound to the data in models/venuesDetail.xml photos attribute.
            url: ListItemData.photos
        }

        Label {
            // The title is bound to the data in models/recipemodel.xml title attribute.
            text: ListItemData.name
            leftMargin: 30
            preferredWidth: 500
            verticalAlignment: VerticalAlignment.Center
            textStyle.fontSize: FontSize.Large
            textStyle.color: Color.Black
        }
    } // Container

    // Highlight function for the highlight Container
    function setHighlight(highlighted) {
        if (highlighted) {
            //highlightContainer.opacity = 0.9;
            listItem.background = Color.create("#75b5d3");
        } else {
            //highlightContainer.opacity = 0.0;
            listItem.background = Color.White;
        }
    }

    // Connect the onActivedChanged signal to the highlight function
    ListItem.onActivationChanged: {
        setHighlight(ListItem.active);
    }

    // Connect the onSelectedChanged signal to the highlight function
    ListItem.onSelectionChanged: {
        setHighlight(ListItem.selected);
    }
    topPadding: 8.0
    leftPadding: 30.0
    rightPadding: 30.0
    bottomPadding: 7.0
}