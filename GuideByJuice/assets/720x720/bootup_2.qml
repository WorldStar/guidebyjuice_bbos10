import bb.cascades 1.0
import my.lib 1.0

Page {
    Container {
        ImageView {
            imageSource: "asset:///images/bg_gbj_promo1.png"
            loadEffect: ImageViewLoadEffect.FadeZoom
            onTouch: {
                if (event.isUp()) eventHandler.ShowHome();
            }
            scalingMethod: ScalingMethod.Fill
        }
        onCreationCompleted: {
            eventHandler.Sleep(3000, 0);
        }
    }
}
