import bb.cascades 1.0
import org.labsquare 1.0

Container {
    id: slidImageItemContainer
    preferredHeight: 200.0
    preferredWidth: 720.0

    topPadding: 20.0
    bottomPadding: 20.0
    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }

}
