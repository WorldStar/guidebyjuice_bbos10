/* Copyright (c) 2012 Research In Motion Limited.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
import bb.cascades 1.0

Container {
    // File path of the pin image
    property string pinImageSource: "asset:///images/pin1.png"
    // pointerOffsetX, pointerOffsetY is the position of the pixel in pin image that should point to the location. Change these to match your pin image.
    property int pointerOffsetX: 64
    property int pointerOffsetY: 112
    /////////////////////////////////////////////////////////
    id: root
    property int idnum: 0
    property string name: ""
    property int index: 0
    property int x: 0
    property int y: 0
    property double lat:0
    property double lon:0
    property alias animDrop: animDrop
    property alias text: idlabel.text
    
    clipContentToBounds: false
    //overlapTouchPolicy: OverlapTouchPolicy.Allow
    layoutProperties: AbsoluteLayoutProperties {
        id: position
        positionX: x - pointerOffsetX
        positionY: y - pointerOffsetY
    }
    layout: DockLayout {

    }

    onTouch:{
        // When an item is selected we push the recipe Page in the chosenItem file attribute.
        if (event.isUp()) {
            if (idnum != 0) {
                console.log("====" + name + "====");

                mapContainer.visible = false;

                eventHandler.onTouchAroundMeItem(aroundmeIndicator, name);
            }
        }
    }
    
    ImageView {
        id: pinImage
        scaleX: .8
        scaleY: .8
        imageSource: pinImageSource
        focusPolicy: FocusPolicy.Touch
        //overlapTouchPolicy: OverlapTouchPolicy.Allow
        onFocusedChanged: {
            if (focused) {
                animFocus.play();
            }
            if (! focused) {
                animUnfocus.play();
            }
        }
        animations: [
            ScaleTransition {
                id: animFocus
                fromX: .8
                toX: 1
                fromY: .8
                toY: 1
                duration: 300
                easingCurve: StockCurve.BounceOut
            },
            ScaleTransition {
                id: animUnfocus
                fromX: 1
                toX: .8
                fromY: 1
                toY: .8
                duration: 300
                easingCurve: StockCurve.BounceOut
            }
        ]
    }
    
    Container {
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Center
        bottomPadding: 20
        Label {
            id: idlabel
            textStyle.fontSize: FontSize.XLarge
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            textStyle.color: Color.White
        }
    }
    
    animations: [
        TranslateTransition {
            id: animDrop
            fromY: - position.positionY
            toY: 0
            duration: 600
            easingCurve: StockCurve.BounceOut
        }
    ]
}
