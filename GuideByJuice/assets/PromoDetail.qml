import bb.cascades 1.0
import org.labsquare 1.0

Page {
    property alias title: promoTitle.text
    property alias desc: promoDesc.text
    property alias logoUrl: logoImage.url

    Container {
        background: Color.DarkCyan

        ScrollView {
            id: scrollView
            scrollViewProperties {
                scrollMode: ScrollMode.Vertical
            }
            Container {
                // merchant logo
                Container {
                    background: Color.Black

                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    WebImageView {
                        id: logoImage
                        scalingMethod: ScalingMethod.AspectFit
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredWidth: 900.0
                        preferredHeight: 600.0
                    }
                }
                Container {
                    leftPadding: 20.0

                    rightPadding: 20.0
                    topPadding: 20.0
                    Label {
                        id: promoTitle
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.fontSize: FontSize.XLarge
                        text: "E-Library Promos"
                        textStyle.color: Color.Green

                    }
                    Label {
                        id: promoDesc
                        multiline: true
                        textStyle.textAlign: TextAlign.Justify
                        textStyle.color: Color.White
                        text: "Draft Beers\\r\\n12pm - 8pm \\r\\nBuy 1 Free 1 Pint\\r\\n\\r\\n8pm - 10pm\\r\\nBuy 1 Free 1/2 Pint\\r\\n\\r\\nBottled Beers\\r\\n12pm - 6pm\\r\\nBuy 4 Free 3 Btls\\r\\n\\r\\n6pm - 8pm\\r\\nBuy 4 Free 2 Btls\\r\\n\\r\\n8pm - 10pm\\r\\nBuy 4 Free 1 Btl\\r\\n-----------------\\r\\nK-Dinner Packages\\r\\n\\r\\nCombo A (2 - 3pax) @ RM188\\r\\n1 x Pizza Margherita\\r\\n1 x BBQ Chicken Wing\\r\\n1 x Chili Sausage\\r\\n1 x Carlsberg Tower\\r\\n\\r\\nCombo B (4 - 6pax) @ RM288\\r\\n1 x Pizza Pepperoni & Salami\\r\\n10pcs x Deep Fried Chicken Wings\\r\\n1 x Crispy Calamori\\r\\n1 xCheesy Nochos\\r\\n2 x Carlsberg Tower\\r\\n\\r\\nCombo C (6-10 pax) @ RM388\\r\\n1 x Pizza Four Seasons\\r\\n1 x Pizza Margherita\\r\\n1 x Sauteed Lamb Cutlers \\r\\n1 x BBQ Chicken Wings\\r\\n1 x French Fries\\r\\n2 x Carlsberg Tower"

                    }

                }
                Container {
                    topPadding: 50.0
                    bottomPadding: 30.0
                    leftPadding: 20.0
                    rightPadding: 20.0
                    // Web
                    Container {
                        background: Color.Black
                        layout: DockLayout {
                        }
                        preferredWidth: 760.0
                        preferredHeight: 143.0
                        Label {
                            id: merchantWeb
                            multiline: true
                            text: "<span style='color:white;'><div><span style='font-size:x-large; font-style:bold'>WEB</span></div><div>carlsbergmalaysia.com.my</div></span>"
                            textFormat: TextFormat.Html
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Left
                            leftMargin: 10.0
                        }
                        ImageView {
                            imageSource: "asset:///images/btn_blk_web.png"
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Right
                        }
                        topMargin: 5.0
                        onTouch: {
                            if (event.isUp()) {
                                //diag.open();
                                invoker.trigger("bb.action.OPEN");
                            }
                        }
                        attachedObjects: [
                            Invocation {
                                id: invoker
                                query {
                                    mimeType: "text/plain"
                                    uri: "http://carlsbergmalaysia.com.my/"
                                }
                            }

                        ]
                        leftPadding: 10.0
                    }
                }
                Container {
                    background: Color.Black
                    leftPadding: 10.0
                    rightPadding: 10.0
                    ScrollView {
                        id: imageScrollView
                        objectName: "imageScrollView"
                        scrollViewProperties.scrollMode: ScrollMode.Horizontal
                    }
                }
            }
        }
    }
}
