import bb.cascades 1.0
import org.labsquare 1.0 // WebImageView

Container {
    layout: DockLayout {
    }
    preferredWidth: 760.0
    preferredHeight: 300.0
    Label {
        id: merchantReview
        multiline: true
        text: "<span><div><span style='font-size:x-large; font-style:bold'>" + "Sector 7 Promos" + "</span></div><div>" + "Special beer prices at special times!" + "</div></span>"
        textFormat: TextFormat.Html
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Left
        leftMargin: 10.0
        preferredWidth: 450.0
        textStyle.color: Color.White
    }
    WebImageView {
        url: "http://iphone.catchacorp.com/media/cache/72/e3/72e3483c9aad946491953269eb87e146.jpg"
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Right
        preferredWidth: 230.0
        scalingMethod: ScalingMethod.AspectFit
    }
    onTouch: {
        if (event.isUp()) {
            var page = promoelib.createObject();
            promoNav.push(page);
        }
    }
    leftPadding: 10.0
    horizontalAlignment: HorizontalAlignment.Center
}
