import bb.cascades 1.0
import my.lib 1.0
import org.labsquare 1.0 // WebImageView
import QtMobility.sensors 1.2 //For MapView
import bb.cascades.maps 1.0 //For MapView
import QtMobilitySubset.location 1.1 //For MapView

Container {
    //map view
    property alias altitude: mapview.altitude
    property alias latitude: mapview.latitude
    property alias longitude: mapview.longitude
    property alias height: mapview.preferredHeight
    property alias width: mapview.preferredWidth
    MapView {
        id: mapview
        altitude: 2000
        latitude: 3.449488
        longitude: 101.406777
        preferredWidth: 768
        preferredHeight: 930

        onRequestRender: {
            if (mapContainer.visible == true) {
                console.log("====request maview rendering====");
                pins.updateMarkers();
            }
        }
    }
}
