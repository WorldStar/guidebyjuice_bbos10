// Tabbed Pane project template
import bb.cascades 1.0
import my.lib 1.0
import org.labsquare 1.0 // WebImageView

import QtMobility.sensors 1.2 //For MapView
import bb.cascades.maps 1.0 //For MapView
import QtMobilitySubset.location 1.1 //For MapView

TabbedPane {
    
    id: motherPane
    
    property alias promo1Title: promo1title.text
    property alias promo2Title: promo2title.text
    property alias promo1Logo: promo1logo.url
    property alias promo2Logo: promo2logo.url
    property alias logoBarOfMonthURL: logoBarOfMonth.url
    property alias logoClubOfMonthURL: logoClubOfMonth.url    
    property bool aroundMeShowing: false

    peekEnabled: false
    showTabsOnActionBar: true
    
    onSidebarStateChanged: {
        if(aroundMeShowing == true && viewContainer.viewContent == true){
            if (sidebarState != 0) {
                console.log("====tab sider bar is shown==="+ sidebarState);
                mainContainer.onAllHide();
            }else{
                eventHandler.Sleep(500, 3);
            }
        }
    }

    attachedObjects: [
        ImagePaintDefinition {
            id: backgroundPaint1
            imageSource: "asset:///images/background.png"
        },
        ImagePaintDefinition {
            id: backgroundPaint
            imageSource: "asset:///images/bootup_02.png"
        },
        ImagePaintDefinition {
            id: backgroundPage
            imageSource: "asset:///images/bg.png"
        },
        ImagePaintDefinition {
            id: titleBar
            imageSource: "asset:///images/bar_top.png"
        },
        ImagePaintDefinition {
            id: searchLocation
            imageSource: "asset:///images/bar_pink_dd.png"
            repeatPattern: RepeatPattern.Y

        },
        ImagePaintDefinition {
            id: promoItemBg
            imageSource: "asset:///images/banner_promos_bg.png"

        },
        ImagePaintDefinition {
            id: promoBackground
            imageSource: "asset:///images/bg_gbj_promo.png"
        }

    ]

    Tab {
        id: tab1Handle
        objectName: "tab1Handle"
        title: qsTr("HOME")
        imageSource: "asset:///images/icon_btn_home.png"
        description: "HOME"

        onTriggered: {
            eventHandler.unlock();
            motherPane.aroundMeShowing = false;
        }

        NavigationPane {
            id: homeNav
            objectName: "homeNav"
            attachedObjects: [
                ComponentDefinition {
                    id: details
                    source: "DetailPage.qml"
                }
            ]

            Page {
                id: tab1

                Container {
                    layout: StackLayout {
                    }
                    background: backgroundPage.imagePaint
                    topPadding: 0.0
                    // title bar
                    ImageView {
                        imageSource: "asset:///images/bar_logo_top.png"

                    }
                    ScrollView {
                        scrollViewProperties.scrollMode: ScrollMode.Vertical
                        Container {
                            leftPadding: 30.0
                            topPadding: 20.0
                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight

                                }

                                onTouch: {
                                    if (event.isUp() && homeNav.count() == 1) {
                                        eventHandler.onTouchBarOfMonth();
                                    }
                                }

                                topPadding: 10.0
                                background: Color.Black
                                ImageView {
                                    imageSource: "asset:///images/banner_main02.png"
                                    preferredHeight: 400.0
                                    preferredWidth: 120.0

                                }

                                Container {
                                    background: Color.Black
                                    preferredHeight: 400.0
                                    preferredWidth: 580.0
                                    WebImageView {
                                        id: logoBarOfMonth

                                        preferredHeight: 400.0
                                        preferredWidth: 580.0

                                    }
                                }
                            }

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight

                                }

                                topMargin: 20.0
                                background: Color.Black
                                onTouch: {
                                    if (event.isUp() && homeNav.count() == 1) {
                                        eventHandler.onTouchClubOfMonth();
                                    }
                                }

                                ImageView {
                                    imageSource: "asset:///images/banner_main01.png"
                                    preferredHeight: 400.0
                                    preferredWidth: 120.0
                                }

                                Container {
                                    background: Color.Black
                                    preferredHeight: 400.0
                                    preferredWidth: 580.0
                                    WebImageView {
                                        id: logoClubOfMonth

                                        preferredHeight: 400.0
                                        preferredWidth: 580.0

                                    }
                                }
                            }
                        }
                    }
                }
            }
            onPopTransitionEnded: {
                // Transition is done destroy the Page to free up memory.
                page.destroy();
            }
        }
    }

    Tab {
        title: qsTr("AROUND ME")
        imageSource: "asset:///images/icon_btn_around.png"
        description: "AROUND ME"
        id: tab2Handle

        onTriggered: {
            eventHandler.onTouchAroundmeTab(aroundmeIndicator);
            motherPane.aroundMeShowing = true;
        }

        NavigationPane {
            id: aroundmeNav
            objectName: "aroundmeNav"
            peekEnabled: false

            onTopChanged: {
                if (viewContainer.viewContent == true) {
                    if (aroundmeNav.count() > 1) {
                        console.log("====nav sider bar is shown===");
                        mainContainer.onAllHide();
                    } else {
                        console.log("====nav sider bar is hidden===");
                        mainContainer.onShowMapView();
                    }
                }
            }

            Page {
                id: rootContainer

                Container {
                    id: aroundme
                    layout: AbsoluteLayout {

                    }
                    background: backgroundPage.imagePaint

                    Container {
	                    layout: DockLayout {
	                    }
	                    background: backgroundPage.imagePaint
	
	                    horizontalAlignment: HorizontalAlignment.Fill
	                    verticalAlignment: VerticalAlignment.Fill
	
	                    Container {
	                        id: mainContainer
	                        topPadding: 110.0
	                        layout: DockLayout {
	
	                        }
	
	                        function onInitAroundMe() {
	                            //request position info
	                            console.log("====request position====");
                                onAllHide();
                                positionSource.update();
	                        }
	
	                        function onShowMapView(){
                                viewContainer.viewContent = true;
                                pointListContainer.visible = false;
                                errLabel.visible = false;
                                mapContainer.visible = true;
                                btList.background = Color.Transparent;
                                btMap.background = Color.Black;
                            }
	                        
	                        function onShowListView(){
                                viewContainer.viewContent = false;
                                pointListContainer.visible = true;
                                errLabel.visible = false;
                                mapContainer.visible = false;
                                btList.background = Color.Black;
                                btMap.background = Color.Transparent;
                            }

	                        function onShowErrorLabel(){
                                pointListContainer.visible = false;
                                errLabel.visible = true;
                                mapContainer.visible = false;
                                btList.background = Color.Black;
                                btMap.background = Color.Transparent;
                            }
	                        
	                        function onAllHide(){
                                pointListContainer.visible = false;
                                errLabel.visible = false;
                                mapContainer.visible = false;
                                btList.background = Color.Transparent;
                                btMap.background = Color.Transparent;
                            }

							//Initialize
	                        onCreationCompleted: {
	                            console.log("====aroundme initialize====");
								//connect signal InitializeAroundMe of eventHandler to onInitAroundMe
	                            eventHandler.InitializeAroundMe.connect(onInitAroundMe);
	                            eventHandler.ShowenMapView.connect(onShowMapView);
                                eventHandler.showErrorLabel.connect(onShowErrorLabel);
                                //add pin showing Me into pinContainer at 0.
	                            pins.showMe();
                            }
	
	                        Container {
	                            id: viewContainer
	                            property bool viewContent: true //true -> mapview, false -> listview
	                            property int itemCount: 0
	                            horizontalAlignment: HorizontalAlignment.Fill
	                            verticalAlignment: VerticalAlignment.Fill
	                            topPadding: 100.0
                                preferredHeight: 1030
                                layout: DockLayout {
	
	                            }
                                Label {
                                    id: errLabel
                                    text: "Sorry, there aren't any \nlisted venues around you."
                                    textStyle.textAlign: TextAlign.Center
                                    enabled: false
                                    multiline: true
                                    textStyle.fontSize: FontSize.XLarge
                                    verticalAlignment: VerticalAlignment.Center
                                    horizontalAlignment: HorizontalAlignment.Center
                                }
	
	                            // Main List
	                            Container {
	                                id: pointListContainer
	                                topPadding: 20.0
	                                leftPadding: 20.0
	                                rightPadding:20.0
	                                ListView {
	                                    id: pointList
	                                    objectName: "pointList"
	
	                                    layout: GridListLayout {
	                                        columnCount: 2
	                                        headerMode: ListHeaderMode.None
	                                        cellAspectRatio: 1.0
	                                        spacingAfterHeader: 0.0
	                                        verticalCellSpacing: 10.0
	                                        spacingBeforeHeader: 0.0
	                                        horizontalCellSpacing: 15.0
	                                    }
	
	                                    listItemComponents: [
	                                        // The stamp Item
	                                        ListItemComponent {
	                                            type: "item"
	                                            AroundMeItem {
	                                            }
	                                        }
	                                    ] // listItemComponents
	
	                                    onTriggered: {
	                                        // When an item is selected we push the recipe Page in the chosenItem file attribute.
	                                        var chosenItem = dataModel.data(indexPath);
	                                        //eventHandler.onTouchAreaItem(areaList);
	
	                                        // Create the content page and push it on top to drill down to it.
	                                        console.log("===="+ chosenItem.name + "====");
	
	                                        eventHandler.onTouchAroundMeItem(aroundmeIndicator, chosenItem.name);
	                                    }
	                                }
	                            }
	
	                            Container {
	                                id: mapContainer
                                    objectName: "mapContainer"
                                    property variant mapview
                                    layout: DockLayout {
	                                }

	                                //pin
	                                Container {
	                                    id: pins
	                                    // Must match the mapview width and height and position
	                                    preferredWidth: 768
	                                    preferredHeight: 930
                                        //touchPropagationMode: TouchPropagationMode.PassThrough
	                                    overlapTouchPolicy: OverlapTouchPolicy.Allow
	                                    property variant me
	
	                                    layout: AbsoluteLayout {
	                                    }
	                                    function addPin(i, idnum, name, lat, lon) {
	                                        console.log("====i: " + i + ", idnum: " + idnum + ", name" + name + ", lat" + lat + ", lon" + lon + "====");
	                                        var marker = pin.createObject();
	                                        marker.idnum = idnum;
	                                        marker.name = name;
	                                        marker.index = i;
	                                        marker.lat = lat;
	                                        marker.lon = lon;
	                                        marker.text = i;
	                                        var xy = eventHandler.worldToPixelInvokable(mapContainer.mapview, marker.lat, marker.lon);
	                                        marker.x = xy[0];
	                                        marker.y = xy[1];
	                                        marker.visible = true;
	                                        pins.add(marker);
	                                        marker.animDrop.play();
	                                        viewContainer.itemCount++;
	                                        
	                                        if(viewContainer.viewContent == true){
	                                            mainContainer.onShowMapView();
	                                        }else {
	                                            mainContainer.onShowListView();
	                                        }
	                                    }
	                                    function removePin(index) {
	                                        var marker = pins.at(index);
	                                        pins.remove(marker);
	                                        viewContainer.itemCount--;

	                                        if(viewContainer.itemCount == 0) {
	                                            mainContainer.onShowErrorLabel();///////////////////////////
	                                        }
	                                    }
	                                    function removeAllPins() {
	                                        //remove old data
	                                        var i = pins.count();
	                                        console.log("====remove pins===="+ i);
	                                        for (i = pins.count() - 1; i > 0; i --) {
	                                            console.log("====removePin" + i + "====");
	                                            pins.removePin(i);
	                                        }
	                                        viewContainer.itemCount = 0;
	                                    }
	                                    function showMe() {
	                                        var marker = pin.createObject();
	                                        marker.pinImageSource = "asset:///images/icon_location_01.png"
	                                        marker.pointerOffsetX = 40
	                                        marker.pointerOffsetY = 40
	                                        pins.insert(-1, marker);
	                                        marker.visible = false;
	                                        me = marker;
	                                    }
	                                    function updateMarkers() {
	                                        eventHandler.updateMarkers(mapContainer.mapview, pins);
	                                    }

	                                    onCreationCompleted: {
                                            console.log("====connect signal addPintoContainer of eventHandler to addPin of pinContainer====");
	                                        //connect signal addPintoContainer of eventHandler to addPin of pinContainer
	                                        eventHandler.addPintoContainer.connect(addPin);
	                                        eventHandler.removePins.connect(removeAllPins);
	                                    }
	                                }
	                            }
	
	                            attachedObjects: [
                                    ComponentDefinition {
	                                    id: mapCompo

                                        Container {
                                            //map view
                                            property alias altitude: mapview.altitude
                                            property alias latitude: mapview.latitude
                                            property alias longitude: mapview.longitude
                                            property alias height: mapview.preferredHeight
                                            property alias width: mapview.preferredWidth
                                            MapView {
                                                id: mapview
                                                altitude: 7000
                                                latitude: 3.449488
                                                longitude: 101.406777
                                                preferredWidth: 768
                                                preferredHeight: 930

                                                onRequestRender: {
                                                    if (mapContainer.visible == true) {
                                                        console.log("====request maview rendering====");
                                                        pins.updateMarkers();
                                                    }
                                                }
                                            }
                                        }
                                    },
	                                ComponentDefinition {
	                                    id: pin
	                                    source: "AroundMePin.qml"
	                                },
	                                PositionSource {
	                                    id: positionSource
	                                    updateInterval: 1000
	                                    active: false

	                                    onPositionChanged: {
	
	                                        fitCenter();
	                                        pins.me.lat = positionSource.position.coordinate.latitude;
	                                        pins.me.lon = positionSource.position.coordinate.longitude;
	                                        console.log("===="+ positionSource.position.coordinate.latitude+"====")
	                                        console.log("========positionUpdated: latitude["+pins.me.lat+"], logitude["+pins.me.lon+"]=====");
	                                        var xy = eventHandler.worldToPixelInvokable(mapContainer.mapview, pins.me.lat, pins.me.lon);
	                                        pins.me.x = xy[0];
	                                        pins.me.y = xy[1];
	                                        pins.me.visible = true;
	                                        rangeSelecter.selectedIndexChanged(0);
	                                        positionSource.active = false;
                                        }
	                                    function fitCenter() {
                                            mapContainer.mapview.latitude = positionSource.position.coordinate.latitude;
                                            mapContainer.mapview.longitude = positionSource.position.coordinate.longitude;
	                                    }
	                                },
	                                Label {
	                                	id: seledVenueName
	                                }
	                            ]
	                        }
	
	                        Container {
	                            id: buttonGroup
	                            horizontalAlignment: HorizontalAlignment.Fill
	                            verticalAlignment: VerticalAlignment.Top
	                            preferredWidth: 768.0
	                            preferredHeight: 100.0
	                            rightPadding: 20.0
	                            leftPadding: 20.0
	                            layout: DockLayout {
	
	                            }
	
	                            background: buttonGroupBg.imagePaint
	
	                            attachedObjects: [
	                                ImagePaintDefinition {
	                                    id: buttonGroupBg
	                                    imageSource: "asset:///images/bar_map_menu.png"
	                                    repeatPattern: RepeatPattern.Fill
	                                }
	                            ]

	                            Container {
	                                id: operationContainer
	                                verticalAlignment: VerticalAlignment.Top
	                                horizontalAlignment: HorizontalAlignment.Right
	                                layout: StackLayout {
	                                    orientation: LayoutOrientation.RightToLeft
	                                }
	
	                                Container {
	                                    id: btMap
	                                    preferredWidth: 100.0
	                                    preferredHeight: 100.0
	                                    verticalAlignment: VerticalAlignment.Center
	
	                                    background: Color.Black
	                                    topPadding: 10.0
	                                    ImageButton {
	                                        onClicked: {
                                                mainContainer.onShowMapView();
	                                        }
	                                        defaultImageSource: "asset:///images/icon_map-top.png"
	                                        pressedImageSource: "asset:///images/icon_map-top.png"
	                                        disabledImageSource: "asset:///images/icon_map-top.png"
	                                        verticalAlignment: VerticalAlignment.Center
	                                        horizontalAlignment: HorizontalAlignment.Center
	                                    }
	                                }
	
	                                Container {
	                                    id: btList
	                                    preferredWidth: 100.0
	                                    preferredHeight: 100.0
	                                    verticalAlignment: VerticalAlignment.Center
	                                    background: Color.Transparent
	
	                                    topPadding: 10.0
	                                    ImageButton {
	                                        onClicked: {
	                                            if (viewContainer.itemCount) {
	                                                mainContainer.onShowListView();
	                                            } else {
                                                    mainContainer.onShowErrorLabel();
                                                }
	                                        }
	                                        defaultImageSource: "asset:///images/icon_map-thumb.png"
	                                        pressedImageSource: "asset:///images/icon_map-thumb.png"
	                                        disabledImageSource: "asset:///images/icon_map-thumb.png"
	                                        verticalAlignment: VerticalAlignment.Center
	                                        horizontalAlignment: HorizontalAlignment.Center
	                                    }
	                                }
	                            }
	                        }
	                    }
	
	                    Container {
	                        preferredHeight: 136.0
	                        leftPadding: 20.0
	                        bottomPadding: 26.0
	                        horizontalAlignment: HorizontalAlignment.Fill
	                        layout: StackLayout {
	                            orientation: LayoutOrientation.LeftToRight
	                        }
	
	                        background: titleBar.imagePaint
	                        Label {
	                            text: "Around Me"
	                            verticalAlignment: VerticalAlignment.Center
                                textStyle.fontSize: FontSize.XLarge
                                textStyle.fontWeight: FontWeight.Bold
                            }
	                    }

                        ActivityIndicator {
                            id: aroundmeIndicator
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Center
                            scaleX: 2.0
                            scaleY: 2.0
                        }

                    }

                    Container {
                        layoutProperties: AbsoluteLayoutProperties {
                            positionX: 20.0
                            positionY: 110.0
                        }

                        background: Color.LightGray
                        // In the DropDown Control, we add a number of options for selecting different values.
                        // We use them later to check how many pints the recipe should make
                        DropDown {
                            id: rangeSelecter
                            preferredWidth: 70.0
                            preferredHeight: 90.0
                            maxHeight: 100.0

                            verticalAlignment: VerticalAlignment.Fill
                            horizontalAlignment: HorizontalAlignment.Left

                            Option {
                                value: 0.5
                                text: "0.5 km"
                                selected: true
                            }
                            Option {
                                value: 1.0
                                text: "1.0 km"
                            }
                            Option {
                                value: 1.5
                                text: "1.5 km"
                            }
                            Option {
                                value: 2.0
                                text: "2.0 km"
                            }
                            Option {
                                value: 2.5
                                text: "2.5 km"
                            }
                            Option {
                                value: 3.0
                                text: "3.0 km"
                            }
                            Option {
                                value: 3.5
                                text: "3.5 km"
                            }
                            Option {
                                value: 4.0
                                text: "4.0 km"
                            }
                            Option {
                                value: 4.5
                                text: "4.5 km"
                            }
                            Option {
                                value: 5.0
                                text: "5.0 km"
                            }

                            //signal whenever item is selected.
                            onSelectedIndexChanged: {
                                // Illustrating usage of DropDown signal handler
                                console.log("====Number of pints changed to " + selectedValue + "====");

                                //move the center position of map showed by mapview to Me.
                                mapContainer.mapview.altitude = 7000;
                                positionSource.position.coordinate.altitude = 7000;
                                positionSource.fitCenter();

                                //request to update AroundMe data using latitude, longitude and range.
                                //Updated data would be get addPin() of pins.
                                console.log("===="+ pins.me.lat+"===="+ pins.me.lon+"====");
                                eventHandler.onChangeAroundMe(aroundmeIndicator, pins.me.lat, pins.me.lon, rangeSelecter.selectedValue);
                            }
                            expanded: false
                        } // DropDown
                    }
                }
            }
        }
    }

    Tab {
        id: tabVenues
        title: qsTr("VENUE")
        imageSource: "asset:///images/icon_btn_venue.png"
        description: "VENUE"

        onTriggered: {
            eventHandler.onTouchVenuesTab(venuesIndicator);
            motherPane.aroundMeShowing = false;
        }

        NavigationPane {
            id: venuesNav
            objectName: "venuesNav"
            Page {
                id: mainVenues

                attachedObjects: [
                    ImagePaintDefinition {
                        id: backgroundPaint3
                        imageSource: "asset:///images/background.png"
                        repeatPattern: RepeatPattern.Fill
                    }
                ]
                Container {
                    id:venuesListContainer
                    background: backgroundPage.imagePaint
                    layout: DockLayout {

                    }
                    // title bar
                    Container {
                        background: titleBar.imagePaint
                        layout: DockLayout {
                        }
                        preferredWidth: 768.0
                        preferredHeight: 136.0
                        leftPadding: 20.0
                        topPadding: 20.0
                        Label {
                            id: title
                            verticalAlignment: VerticalAlignment.Top
                            horizontalAlignment: HorizontalAlignment.Left
                            text: "Venues"
                            textStyle.fontSize: FontSize.XLarge
                            textStyle.fontWeight: FontWeight.Bold
                        }
                    }
                    Container {
                        // A paper-style image is used to tile the background.

                        layout: DockLayout {

                        }

                        topPadding: 136.0
                        leftPadding: 20.0
                        rightPadding: 20.0
                        horizontalAlignment: HorizontalAlignment.Fill
                        // Main List
                        ListView {
                            id: areaList
                            objectName: "areaList"

                            layout: StackListLayout {
                                headerMode: ListHeaderMode.None
                                orientation: LayoutOrientation.TopToBottom
                            }

                            // This data model will be replaced by a JSON model when the application starts,
                            // an XML model can be used to prototype the UI and for smaller static lists.
                            //                dataModel: XmlDataModel {
                            //                    source: "models/areas.xml"
                            //                }

                            listItemComponents: [
                                ListItemComponent {
                                    type: "item"
                                    AreaItem {

                                    }
                                }
                            ] // listItemComponents
                            horizontalAlignment: HorizontalAlignment.Fill

                            onTriggered: {
                                // When an item is selected we push the recipe Page in the chosenItem file attribute.
                                var chosenItem = dataModel.data(indexPath);
                                //eventHandler.onTouchAreaItem(areaList);

                                // Create the content page and push it on top to drill down to it.
                                seledAreaId.text = chosenItem.id;
                                seledAreaName.text = chosenItem.name;//areaName.text;

                                eventHandler.onTouchAreaItem(venuesIndicator, seledAreaId, seledAreaName);
                            }
                        }
                    } // Container
                    ActivityIndicator {
                        id: venuesIndicator
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        scaleX: 2.0
                        scaleY: 2.0
                    }
                }
            } // AreaPage

            onPopTransitionEnded: {
                // Transition is done destroy the Page to free up memory.
                page.destroy();
            }
            attachedObjects: [
                Label {
                    id: seledAreaId

                },
                Label {
                    id: seledAreaName

                }
            ]
        } // Navigation Page
    }

    Tab {
        id: tabSearch
        title: qsTr("SEARCH")
        imageSource: "asset:///images/icon_btn_search.png"
        description: "SEARCH"
        onTriggered: {
            eventHandler.onTouchSearchTab();
            motherPane.aroundMeShowing = false;
        }
        NavigationPane {
            id: searchNav
            objectName: "searchNav"
            Page {
                Container {
                    id: maincontainer
                    layout: DockLayout {

                    }
                    background: backgroundPage.imagePaint

                    // title bar
                    Container {
                        background: titleBar.imagePaint
                        layout: DockLayout {
                        }
                        preferredWidth: 768.0
                        preferredHeight: 136.0
                        leftPadding: 20.0
                        topPadding: 20.0
                        Label {
                            id: titleLabel
                            verticalAlignment: VerticalAlignment.Top
                            horizontalAlignment: HorizontalAlignment.Left
                            text: "Search"
                            textStyle.fontSize: FontSize.XLarge
                            textStyle.fontWeight: FontWeight.Bold
                        }
                    }
                    Container {
                        id: searchbar
                        layout: StackLayout {
                        }
                        preferredHeight: 230.0
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Fill
                        leftPadding: 20.0
                        rightPadding: 20.0
                        topPadding: 150.0
                        bottomMargin: 0.0

                        Container {
                            background: Color.Gray
                            topPadding: 1.0
                            leftPadding: 1.0
                            rightPadding: 1.0
                            bottomPadding: 1.0
                            Container {
                                topMargin: 40.0
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                background: Color.White
                                translationX: 0.0
                                translationY: 0.0

                                leftPadding: 20.0
                                rightPadding: 20.0
                                TextField {
                                    id: token
                                    textFormat: TextFormat.Plain
                                    backgroundVisible: false
                                    leftMargin: 0.0
                                    onTextChanged: {
                                        eventHandler.onSearch(searchIndicator,token);
                                    }
                                    onFocusedChanged: {
                                        if (focused) location.setSelectedIndex(-1);
                                    }
                                    hintText: "Enter keyword here"
                                }
                                ImageButton {
                                    defaultImageSource: "asset:///images/icon_search.png"
                                    verticalAlignment: VerticalAlignment.Center
                                    onClicked: {
                                        eventHandler.onSearch(searchIndicator,token);
                                        
                                    }
                                    rightMargin: 0.0

                                }

                            }
                        }
                        Container {
                            topPadding: 30.0
                            DropDown {
                                id: location
                                title: "Location"

                                objectName: "locationDropBox"
                                onSelectedIndexChanged: {
                                    console.log("SelectedIndex was changed to " + selectedIndex);
                                    if(selectedIndex >= 0){
	                                    var opt = location.selectedOption;
	
	                                    eventHandler.onSelectArea(searchIndicator, opt);
	                                }
                                }
                            }

                        }
                    }
                    ActivityIndicator {
                        id: searchIndicator
                        verticalAlignment: VerticalAlignment.Bottom
                        horizontalAlignment: HorizontalAlignment.Center
                        scaleX: 2.0
                        scaleY: 2.0
                    }
                    Container {
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Bottom
                        bottomPadding: 100.0

                        ImageButton {
                            defaultImageSource: "asset:///images/search_btn.jpg"
                            pressedImageSource: "asset:///images/search_btn_clicked.png"
                            disabledImageSource: "asset:///images/search_btn_clicked.png"

                            onClicked: {
                                eventHandler.onSearch(searchIndicator, token);
                            }
                        }

                    }
                }
            }
        }

    }

    Tab {
        title: qsTr("PROMO")
        imageSource: "asset:///images/icon_btn_promo.png"
        description: "PROMO"
        onTriggered: {
            eventHandler.onTouchPromoTab(promoIndicator);
            motherPane.aroundMeShowing = false;
        }

        NavigationPane {
            id: promoNav
            objectName: "promoNav"
            Page {
                attachedObjects: [
                    Label {
                        visible: false
                        id: seledPromoNum
                    }
                ]
                Container {
                    background: promoBackground.imagePaint
                    topPadding: 50.0
                    bottomPadding: 50.0
                    leftPadding: 10.0
                    rightPadding: 10.0
                    layout: DockLayout {

                    }
                    ScrollView {
                        scrollViewProperties.scrollMode: ScrollMode.Vertical
                        Container {
                            topPadding: 10.0
                            bottomPadding: 10.0
                            leftPadding: 10.0
                            rightPadding: 10.0
                            Container {
                                layout: StackLayout {

                                }
                                horizontalAlignment: HorizontalAlignment.Center
                                topPadding: 40.0
                                bottomPadding: 30.0
                                ImageView {
                                    imageSource: "asset:///images/Logo_Carlsberg480.png"
                                    verticalAlignment: VerticalAlignment.Center
                                    horizontalAlignment: HorizontalAlignment.Center
                                    preferredWidth: 400.0
                                    scalingMethod: ScalingMethod.AspectFit

                                }

                            }
                            Container {
                                id: grayTransparentContainer
                                background: promoItemBg.imagePaint
                                preferredHeight: 500.0
                                topPadding: 20.0
                                leftPadding: 20.0
                                bottomPadding: 20.0
                                rightPadding: 20.0
                                preferredWidth: 900.0
                                Container {
                                    bottomPadding: 10.0
                                    Label {
                                        id: promoTitle
                                        text: "BEER-O-CLOCK SPECIALS!"
                                        textStyle.fontSize: FontSize.XLarge
                                        textStyle.color: Color.White
                                    }
                                }
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight

                                    }
                                    preferredWidth: 760.0
                                    preferredHeight: 300.0
                                    WebImageView {
                                        id: promo1logo
                                        preferredWidth: 200.0
                                        //scalingMethod: ScalingMethod.AspectFit
                                        preferredHeight: 150.0
                                    }
                                    Label {
                                        id: promo1title
                                        multiline: true
                                        text: ""
                                        textFormat: TextFormat.Html
                                        leftMargin: 10.0
                                        preferredWidth: 450.0
                                        textStyle.color: Color.White
                                    }
                                    onTouch: {
                                        if (event.isUp()) {
                                            seledPromoNum.text = "1";
                                            eventHandler.onTouchPromo(seledPromoNum);
                                        }
                                    }
                                    leftPadding: 10.0
                                    horizontalAlignment: HorizontalAlignment.Center
                                }
                                Divider {

                                }
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight

                                    }
                                    preferredWidth: 760.0
                                    preferredHeight: 300.0
                                    WebImageView {
                                        id: promo2logo
                                        preferredWidth: 200.0
                                        //scalingMethod: ScalingMethod.AspectFit
                                        preferredHeight: 150.0
                                    }
                                    Label {
                                        id: promo2title
                                        multiline: true
                                        textFormat: TextFormat.Html
                                        leftMargin: 10.0
                                        preferredWidth: 450.0
                                        textStyle.color: Color.White
                                    }
                                    onTouch: {
                                        if (event.isUp()) {
                                            seledPromoNum.text = "2";
                                            eventHandler.onTouchPromo(seledPromoNum);
                                        }
                                    }
                                    leftPadding: 10.0
                                    horizontalAlignment: HorizontalAlignment.Center
                                }
                            }
                            Container {
                                topPadding: 50.0
                                bottomPadding: 20.0
                                // Web
                                Container {
                                    background: Color.create("#ffe3e3e3")
                                    layout: DockLayout {
                                    }
                                    preferredWidth: 760.0
                                    preferredHeight: 143.0
                                    Label {
                                        id: promoWeb
                                        multiline: true
                                        text: "<span><div><span style='font-size:x-large; font-style:bold'>WEB</span></div><div>carlsbergmalaysia.com.my</div></span>"
                                        textFormat: TextFormat.Html
                                        verticalAlignment: VerticalAlignment.Center
                                        horizontalAlignment: HorizontalAlignment.Left
                                        leftMargin: 10.0
                                    }
                                    ImageView {
                                        imageSource: "asset:///images/btn_blk_web.png"
                                        verticalAlignment: VerticalAlignment.Center
                                        horizontalAlignment: HorizontalAlignment.Right
                                    }
                                    topMargin: 5.0
                                    onTouch: {
                                        if (event.isUp()) {
                                            //diag.open();
                                            invoker.trigger("bb.action.OPEN");
                                        }
                                    }
                                    attachedObjects: [
                                        Invocation {
                                            id: invoker
                                            query {
                                                mimeType: "text/plain"
                                                uri: "http://carlsbergmalaysia.com.my/"
                                            }
                                        }

                                    ]
                                    leftPadding: 10.0
                                }
                            }
                            // Facebook
                            Container {
                                topPadding: 20.0
                                bottomPadding: 20.0
                                Container {
                                    background: Color.create("#ffe3e3e3")
                                    layout: DockLayout {
                                    }
                                    preferredWidth: 760.0
                                    preferredHeight: 143.0
                                    Label {
                                        id: merchantfb
                                        multiline: true
                                        text: "<span><div><span style='font-size:x-large; font-style:bold'>FACEBOOK</span></div><div>facebook.com/Carlsberg.MY</div></span>"
                                        textFormat: TextFormat.Html
                                        verticalAlignment: VerticalAlignment.Center
                                        horizontalAlignment: HorizontalAlignment.Left
                                        leftMargin: 10.0
                                    }
                                    ImageView {
                                        imageSource: "asset:///images/btn_blk_facebook.png"
                                        verticalAlignment: VerticalAlignment.Center
                                        horizontalAlignment: HorizontalAlignment.Right
                                    }
                                    topMargin: 5.0
                                    onTouch: {
                                        if (event.isUp()) {
                                            //diag.open();
                                            fbInvoker.trigger("bb.action.OPEN");
                                        }
                                    }
                                    attachedObjects: [
                                        Invocation {
                                            id: fbInvoker
                                            query {
                                                mimeType: "text/plain"
                                                uri: "http://facebook.com/Carlsberg.MY"
                                            }
                                        }

                                    ]
                                    leftPadding: 10.0
                                }
                            }
                        }
                    }

                    ActivityIndicator {
                        id: promoIndicator
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        scaleX: 2.0
                        scaleY: 2.0
                    }
                }
            }
        }

        onCreationCompleted: {
            // this slot is called when declarative scene is created
            // write post creation initialization here
            console.log("TabbedPane - onCreationCompleted()")

            // enable layout to adapt to the device rotation
            // don't forget to enable screen rotation in bar-bescriptor.xml (Application->Orientation->Auto-orient)
            OrientationSupport.supportedDisplayOrientation = SupportedDisplayOrientation.All;

            console.log("====create mapview====");
            var map = mapCompo.createObject();
            console.log("====insert mapview to mapcontainer====");
            mapContainer.insert(-1, map);
            console.log("====substitute mapview to mapview of mapcontainer====");
            mapContainer.mapview = map;
            console.log("====substituted mapview====");
        }
    }
}