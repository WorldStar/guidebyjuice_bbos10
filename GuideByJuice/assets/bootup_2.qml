import bb.cascades 1.0
import my.lib 1.0

Page {
    Container {
        ImageView {
            imageSource: "asset:///images/bootup_02.png"
            loadEffect: ImageViewLoadEffect.FadeZoom
            onTouch: {
		if (event.isUp()) eventHandler.ShowHome();
            }

        }
        onCreationCompleted: {
            eventHandler.Sleep(5000, 0);
        }
    }
}
