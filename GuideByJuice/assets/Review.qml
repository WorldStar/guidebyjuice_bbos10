import bb.cascades 1.0
import org.labsquare 1.0

Page {
    property alias phoneNumberText: merchantReview.text
    property alias title: merchantName.text
    property alias review: merchantReview.text
    property alias logoUrl: logoImage.url

    Container {
        background: backgroundPage.imagePaint

        attachedObjects: [
            ImagePaintDefinition {
                id: backgroundPage
                imageSource: "asset:///images/bg.png"
            },
            ImagePaintDefinition {
                id: titleBar
                imageSource: "asset:///images/bar_top.png"
            }
        ]
        // title bar
        Container {
            background: titleBar.imagePaint
            layout: DockLayout {
            }
            preferredWidth: 768.0
            preferredHeight: 136.0
            leftPadding: 20.0
            topPadding: 20.0
            layoutProperties: AbsoluteLayoutProperties {

            }
            Label {
                id: title
                verticalAlignment: VerticalAlignment.Top
                horizontalAlignment: HorizontalAlignment.Left
                text: "Reviews"
                textStyle.fontSize: FontSize.XLarge
                textStyle.fontWeight: FontWeight.Bold
            }
        }
        
        ScrollView {
            id: scrollView
            scrollViewProperties {
                scrollMode: ScrollMode.Vertical
            }
            Container {
                // merchant logo
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    topPadding: 10.0
                    bottomPadding: 10.0
                    WebImageView {
                        id: logoImage
                        scalingMethod: ScalingMethod.AspectFit
                        preferredHeight: 500.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                }
                Container {
                    leftPadding: 20.0
                    rightPadding: 20.0
                    bottomPadding: 50.0
                    
                    Label {
	                    id: merchantName
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.fontSize: FontSize.XXLarge

                    }
	                Label {
	                    id: merchantReview
                        multiline: true
                        textStyle.textAlign: TextAlign.Justify

                    }

				}
            }
        }
    }
}
