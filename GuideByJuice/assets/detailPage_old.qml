import bb.cascades 1.0
Page {
    id: detailPage
    Container {
        background: backgroundPage.imagePaint

        layout: StackLayout {
            orientation: LayoutOrientation.TopToBottom
        }

        attachedObjects: [
            ImagePaintDefinition {
                id: backgroundDetails
                imageSource: "asset:///images/bar_top.png"
            },
            ImagePaintDefinition {
                id: backgroundfill
                imageSource: "asset:///images/bar_fill.png"
            }
        ]

        ScrollView {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            scrollViewProperties {
                scrollMode: ScrollMode.Vertical
            }

            Container {

                Container {

                    background: backgroundDetails.imagePaint
                    preferredWidth: 768.0
                    preferredHeight: 136.0
                    layout: DockLayout {
                    }
                    Label {
                        text: "Details"
                        verticalAlignment: VerticalAlignment.Top
                        textStyle.fontSize: FontSize.Large
                        textStyle.color: Color.Black
                    }
                }

                ImageView {
                    imageSource: "asset:///images/img_detail.png"
                    preferredWidth: 768.0
                    preferredHeight: 492.0
                }

                Label {
                    text: "Circus Bar & Lounge"
                    textStyle.fontSize: FontSize.Large
                    textStyle.color: Color.Black
                }

                Label {
                    text: "Level 3, Fashion Avenue,"
                    textStyle.fontSize: FontSize.Small
                    textStyle.color: Color.Black
                }

                Label {
                    text: "Pavilion, Kuala Lumpur"
                    textStyle.fontSize: FontSize.Small
                    textStyle.color: Color.Black
                }

                Container {
                    layout: AbsoluteLayout {
                    }
                    bottomMargin: 20.0
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center

                    preferredWidth: 750.0
                    preferredHeight: 138.0
                    background: backgroundfill.imagePaint

                    Container {
                        layout: DockLayout {
                        }
                        Label {
                            text: "REVIEW"
                            textStyle.fontSize: FontSize.Large
                            textStyle.color: Color.Black
                        }

                        Label {
                            text: "Impressions of this venue"
                            textStyle.fontSize: FontSize.Small
                            textStyle.color: Color.Black
                        }

                    }

                    ImageView {
                        imageSource: "asset:///images/btn_blk_review.png"
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Right
                        translationX: 612.0
                    }
                }

                Container {
                    layout: AbsoluteLayout {
                    }
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    bottomMargin: 20.0

                    preferredWidth: 750.0
                    preferredHeight: 138.0
                    background: backgroundfill.imagePaint

                    Container {
                        layout: DockLayout {
                        }
                        Label {
                            text: "PHONE"
                            textStyle.fontSize: FontSize.Large
                            textStyle.color: Color.Black
                        }

                        Label {
                            text: "03 2341 6151"
                            textStyle.fontSize: FontSize.Small
                            textStyle.color: Color.Black
                        }

                    }

                    ImageView {
                        imageSource: "asset:///images/btn_blk_call.png"
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Right
                        translationX: 612.0
                    }
                }

                Container {
                    layout: AbsoluteLayout {
                    }
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    bottomMargin: 20.0

                    preferredWidth: 750.0
                    preferredHeight: 138.0
                    background: backgroundfill.imagePaint

                    Container {
                        layout: DockLayout {
                        }
                        Label {
                            text: "WEB"
                            textStyle.fontSize: FontSize.Large
                            textStyle.color: Color.Black
                        }

                        Label {
                            text: "http://circus.com.my"
                            textStyle.fontSize: FontSize.Small
                            textStyle.color: Color.Black
                        }

                    }

                    ImageView {
                        imageSource: "asset:///images/btn_blk_web.png"
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Right
                        translationX: 612.0
                    }
                }

                Container {
                    layout: AbsoluteLayout {
                    }
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    bottomMargin: 20.0

                    preferredWidth: 750.0
                    preferredHeight: 138.0
                    background: backgroundfill.imagePaint

                    Container {
                        layout: DockLayout {
                        }
                        Label {
                            text: "TAKE ME THERE"
                            textStyle.fontSize: FontSize.Large
                            textStyle.color: Color.Black
                        }

                    }

                    ImageView {
                        imageSource: "asset:///images/btn_blk_there-me.png"
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Right
                        translationX: 612.0
                    }
                }

                Container {
                    layout: AbsoluteLayout {
                    }
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    bottomMargin: 20.0

                    preferredWidth: 750.0
                    preferredHeight: 138.0
                    background: backgroundfill.imagePaint

                    Container {
                        layout: DockLayout {
                        }
                        Label {
                            text: "BLACKBERRY MESSENGER"
                            textStyle.fontSize: FontSize.Large
                            textStyle.color: Color.Black
                        }

                    }

                    ImageView {
                        imageSource: "asset:///images/btn_blk_bbm.png"
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Right
                        translationX: 612.0
                    }
                }

                Container {
                    layout: AbsoluteLayout {
                    }
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    bottomMargin: 20.0

                    preferredWidth: 750.0
                    preferredHeight: 138.0
                    background: backgroundfill.imagePaint

                    Container {
                        layout: DockLayout {
                        }
                        Label {
                            text: "TWEET IT"
                            textStyle.fontSize: FontSize.Large
                            textStyle.color: Color.Black
                        }

                    }

                    ImageView {
                        imageSource: "asset:///images/btn_blk_twitter.png"
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Right
                        translationX: 612.0
                    }
                }

                Container {
                    layout: AbsoluteLayout {
                    }

                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center

                    preferredWidth: 750.0
                    preferredHeight: 138.0
                    background: backgroundfill.imagePaint
                    bottomMargin: 20.0
                    Container {
                        layout: DockLayout {
                        }
                        Label {
                            text: "SHARE IT ON FACEBOOK"
                            textStyle.fontSize: FontSize.Large
                            textStyle.color: Color.Black
                        }

                    }

                    ImageView {
                        imageSource: "asset:///images/btn_blk_facebook.png"
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Right
                        translationX: 612.0
                    }
                }

            }
        }

    }
}
