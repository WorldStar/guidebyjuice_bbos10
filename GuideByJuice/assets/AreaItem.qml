import bb.cascades 1.0

Container {
    id: venueContainer
    layout: DockLayout {
    }

    Container {
        horizontalAlignment: HorizontalAlignment.Center
        preferredWidth: 760
        preferredHeight: 120

        layout: DockLayout {
        }
        // The Item content an image and a text
        Container {
            id: listItem
            leftPadding: 20
            horizontalAlignment: HorizontalAlignment.Fill
            preferredWidth: 760
            preferredHeight: 100

            background: itembg.imagePaint

            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            Label {
                id: areaName
                // The title is bound to the data in models/recipemodel.xml title attribute.
                text: ListItemData.name
                textStyle.color: Color.create("#FFFFFF")
                textStyle.fontSize: FontSize.XLarge
                verticalAlignment: VerticalAlignment.Center

            } // Label
        } // Container
    } // Container

    // Highlight function for the highlight Container
    function setHighlight(highlighted) {
        if (highlighted) {
            //highlightContainer.opacity = 0.9;
            listItem.background = itembg;
        } else {
            //highlightContainer.opacity = 0.0;
            listItem.background = itembg;
        }
    }

    // Connect the onActivedChanged signal to the highlight function
    ListItem.onActivationChanged: {
        setHighlight(ListItem.active);
    }

    // Connect the onSelectedChanged signal to the highlight function
    ListItem.onSelectionChanged: {
        setHighlight(ListItem.selected);
    }
    attachedObjects: [
        ImagePaintDefinition {
            id: itembg
            imageSource: "asset:///images/bar_pink.png"

        }
    ]
}
